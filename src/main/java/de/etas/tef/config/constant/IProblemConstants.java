package de.etas.tef.config.constant;

public interface IProblemConstants 
{
	public static final String TXT_CHECKER_DUPLICATED_PROBLEM = "Duplicated Problem";
	public static final String TXT_CHECKER_MISSING_ITEM_PROBLEM = "Missing Item Problem";
	public static final String TXT_CHECKER_OBSOLETE_PROBLEM = "Obsolete Problem";
	public static final String TXT_CHECKER_UNKNOWN_PROBLEM = "Unknown Problem";
	
	public static final String TXT_PROBLEM_DUPLICATED_KEY = "Duplicated Key";
	public static final String TXT_PROBLEM_DUPLICATED_SECTION = "Duplicated Section";
	public static final String TXT_PROBLEM_MISSING_SECTION = "Missing Section";
	public static final String TXT_PROBLEM_MISSING_KEY = "Missing Key";
	public static final String TXT_PROBLEM_OBSOLETE_SECTION = "Obsolete Section";
	public static final String TXT_PROBLEM_OBSOLETE_KEY = "Obsolete Key";
	public static final String TXT_PROBLEM_UNKNOWN_SECTION = "Unknown Section";
	public static final String TXT_PROBLEM_UNKNOWN_KEY = "Unknown Key";
	
	
	public static final int SEL_DUPLICATE = 1;
	public static final int SEL_MISSING = SEL_DUPLICATE<<1;
	public static final int SEL_OBSOLETE = SEL_MISSING<<1;
	public static final int SEL_UNKNOWN = SEL_OBSOLETE<<1;
	public static final int SEL_NONE = SEL_UNKNOWN << 1;
	
	public static final int PROBLEM_DUPLICATED_SECTION = 0x00;
	public static final int PROBLEM_DUPLICATED_KEY = 0x01;
	public static final int PROBLEM_MISSING_KEY = 0x02;
	public static final int PROBLEM_MISSING_BLOCK = 0x03;
	public static final int PROBLEM_OBSOLETE_BLOCK = 0x04;
	public static final int PROBLEM_OBSOLETE_KEY = 0x05;
	public static final int PROBLEM_UNKNOWN_BLOCK = 0x06;
	public static final int PROBLEM_UNKNOWN_KEY = 0x07;
}
