package de.etas.tef.config.constant;

public interface IFileConstants
{
	public static final String FILE_DATA_TYPE_XML = "./template/type.xml";
	public static final String FILE_DATA_TEMPLATE_MCD_INI = "./template/MCD.ini";
	public static final String DIR_TEMPLATE = "src/template";
	public static final String FILE_XML_EXTENSION = ".xml";
	public static final String FILE_DIR_RELEASE = "./template/";
	public static final String FILE_DIR_DEV = "D:\\SubVersion\\ConfigFileTool\\release\\V1.1.0\\template\\";
}
