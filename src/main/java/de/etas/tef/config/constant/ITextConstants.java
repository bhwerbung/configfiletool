package de.etas.tef.config.constant;

public interface ITextConstants
{
	public static final String TXT_VERSION = "1.3.0";
	public static final String TXT_APP_TITLE = "Config File Tool / Version " + TXT_VERSION + " / TEF / ETAS GmbH";
	public static final String TXT_LABEL_FILE = "File";
	public static final String TXT_LABEL_TARGET = "Target";
	public static final String TXT_LABEL_BLOCK_LIST = "Blocks:";
	public static final String TXT_DIRECTORY = "Directory ";
	public static final String TXT_BTN_SELECT = "Select";
	public static final String TXT_BTN_RUN = "Run";
	public static final String TXT_BTN_ADD = "Add";
	public static final String TXT_BTN_DELETE = "Delete";
	public static final String TXT_BTN_SAVE = "Output";
	public static final String TXT_PM_NUMBER = "PM_NUMBER";
	public static final String TXT_BTN_CONNECT = "Connect";
	public static final String TXT_BTN_ACCEPT_SOURCE = "Accept";
	public static final String TXT_BTN_LEFT = "Left";
	public static final String TXT_BTN_RIGHT = "Right";
	public static final String TXT_GPIB_DEVICE_NAME = "DEVICE_NAME";
	public static final String TXT_GPIB_PM_NUMBER = "PM_NUMBER";
	public static final String TXT_GPIB_ADRESS = "GPIB_ADRESS";
	public static final String TXT_CONFIG_FILE = "Config File";
	public static final String TXT_LOCK_EDITING = "Edit Lock";
	public static final String TXT_TEMP = "TEMP";
	public static final String[] TXT_TABLE_TITLES = {"Name", "Value"};
	public static final String TXT_COPY = "Copy";
	public static final String TXT_PASTE = "Paste";
	public static final String TXT_INI_FILE_DEFAULT = "DEFAULT";
	public static final String TXT_REPOSITORY_FILES = "Repository Files";
	public static final String TXT_CONFIG_FILES = "INI Files";
	public static final String TXT_MENU_COPY = "Copy";
	public static final String TXT_MENU_PASTE = "Paste";
	public static final String TXT_MENU_ADD_DIR = "Add Directory";
	public static final String TXT_MENU_ADD_FILE = "Add File";
	public static final String TXT_MENU_DELETE = "Delete";
	public static final String TXT_REPOSITORY_TEXT = "Replace Text";
	public static final String TXT_REPLACE = "Replace ";
	public static final String TXT_BROWSE = "Browse...";
	public static final String TXT_OK = "OK";
	public static final String TXT_CANCEL = "Cancel";
	public static final String TXT_DEFAULT_DIR = "DEFAULT";
	public static final String TXT_DEFAULT_INI = "default.ini";
	public static final String TXT_DEFAULT_FILE = "default.txt";
	public static final String TXT_REPOSITORY_README_FILE = "README.md";
	public static final String TXT_FILE_HISTORY = "File History"; 
	public static final String TXT_COMMITS = "Commits"; 
	public static final String TXT_TOOLBAR_ADD_FILE = "Add File";
	public static final String TXT_TOOLBAR_ADD_DIR = "Add Directory";
	public static final String TXT_TOOLBAR_FILE_HISTORY = "File History";
	public static final String TXT_TOOLBAR_COMMIT_HISTORY = "Commit History";
	public static final String[] TXT_ARRAY_TABLE_HISTORY_HEADER = {"Time", "Comments", "User"};
	public static final String[] TXT_ARRAY_TABLE_COMMITS_HEADER = {"Time", "Comments", "User"};
	public static final String TXT_SEARCH_FILE_NAME = "Search File Name";
	public static final String TXT_SEARCH_FILE_CONTENT_COMPLETE = "Search File Content Complete";
	public static final String TXT_SEARCH = "Search";
	public static final String TXT_TEMPLATE = "Template: ";
	public static final String TXT_UNKNOWN = "UNKNOWN";
	public static final String TXT_EMPTY_STRING = "";
	public static final String TXT_SECTION = "Section";
	public static final String TXT_KEY = "Key";
	public static final String TXT_VALUE = "Value";
	public static final String TXT_SEARCH_CONDITION = "Search Conditions: ";
	public static final String TXT_FILE_NAME = "File Name";
	public static final String TXT_DUPLICATED_SECTION = "Check Duplicate Items";
	public static final String TXT_MISSING_ITEM = "Check Missing Items";
	public static final String TXT_OBSOLETE_ITEM = "Check Obsolete Items";
	public static final String TXT_UNKNOWN_ITEM = "Check Unknown Items";
	public static final String TXT_VALIDATION_OPTIONS = "Validation Options: ";
	public static final String TXT_RELOAD_TEMPLATE = "Reload Tempate Files";
	public static final String TXT_SET_SEARCH_PATH = "Set Search Path";
	public static final String[] TXT_EMPTY_STRING_ARRAY = {};
	public static final String TXT_LAST_MODIFIED_DATE = "Last Modified";
	public static final String TXT_ATTR_MUST_HAVE = "musthave";
	public static final String TXT_ATTR_OBSOLETE = "obsolete";
	public static final String TXT_CHECKER_FILE_NAME = "CheckerFileName";
	public static final String TXT_SETTING_FILE = "settings.json";
	public static final String[] TXT_CONFIG_FILE_EXTENSION = {"*.ini", "*.*"};
	public static final String[] TXT_CONFIG_FILE_NAME = {"Config File (*.ini)", "All Files (*.*)"};
	public static final String[] TXT_PATTERN = {"ini"};
	public static final String[] TXT_SUPPORT_FILE_TYPE = {".ini", ".txt"};
	public static final String TXT_SELECT_TEMPLATE = "SELECT TEMPLATE !";
	public static final String TXT_SELECT_DIR = "SELECT SEARCH DIRECTORY !";
	public static final String TXT_TITLE_NO_FILE_SELECTED = "Warning - No File Selected";
	public static final String TXT_CONTENT_NO_FILE_SELECTED = "No Config Files are seledted in table !\n\nPlease: \n-> 1. Search config files by clicking SEARCH button\n\rand\n\r-> 2. Select Config files in the table";
	public static final String TXT_TITLE_NO_CHECKER_SELECTED = "Warning - No Checker Selected";
	public static final String TXT_CONTENT_NO_CHECKER_SELECTED = "No Checker are seledted !\n\nPlease: \n-> Select one checker";
	public static final String TXT_TITLE_EXIT = "Confirm - Exit Confirmation";
	public static final String TXT_CONTENT_EXIT = "Do you really want to Exit?";
	public static final String TXT_CHECKER_TAB_NAME = "Checker";
	public static final String TXT_REPLACE_TAB_NAME = "Edit Config File";
	public static final String TXT_FUNCTION_TAB_NAME = "Sort";
	public static final String TXT_DETAIL_SEARCH_TAB_NAME = "Detail Search";
	public static final String TXT_TABLE_RIGHT_MENU_REORDER_CONFIG_BLOCKS = "Reorder Config Blocks";
	public static final String TXT_REMOVE_COMMENT_1 = "--=\"\"";
	public static final String TXT_TABLE_RIGHT_MENU_REMOVE_COMMENTS = "Remove   " + TXT_REMOVE_COMMENT_1;
	public static final String TXT_TITLE_REMOVE_LINE = "Confirm - Remove Line";
	public static final String TXT_CONTENT_REMOVE_LINE = "Do you want to remove following text in selected files ?\n\n";
	public static final String TXT_BTN_CHECKER_RUN = "Run Checker";
	public static final String TXT_BTN_FUNCTION_RUN = "Run";
	public static final String TXT_TITLE_RESORT_BLOCKS = "Confirm - Resort Config Blocks";
	public static final String TXT_CONTENT_RESORT_BLOCKS = "Do you want to resort the config blocks in selected files ?";
	public static final String TXT_TITLE_NO_FUNCTION_SELECTED = "Warning - No Function Selected";
	public static final String TXT_CONTENT_NO_FUNCTION_SELECTED = "No Function are seledted !\n\nPlease select on function to run";
	public static final String TXT_BTN_CLEAR_ALL = "Clear All";
	public static final String TXT_BTN_SELECT_ALL = "Select All";
	public static final String TXT_BTN_RELOAD = "Reload Files";
	public static final String TXT_FILE_TABLE_CONTENT = "Config Files found in working directory. Please select the files you want to check, sort or edit";
}
