package de.etas.tef.config.constant;

import java.text.SimpleDateFormat;

public interface IConstants
{
//	GUI Constants Block
	public static final int MAIN_SCREEN_WIDTH = 1000;
	public static final int MAIN_SCREEN_HEIGHT = 800;
	
	public static final int BTN_DEFAULT_WIDTH = 100;
	public static final int LABEL_DEFAULT_WIDTH = 45;
	public static final int HEIGHT_HINT = 150;
	public static final int SEARCH_TEXT_HEIGHT = 24;
	public static final int EMPTY_INT = -1;
	
	public static final int FOCUS_NONE = 0x00;
	public static final int FOCUS_BLOCK = 0x01;
	public static final int FOCUS_PARAMETER = 0x02;
	public static final int DATA_TYPE_DIR = 0x00;
	public static final int DATA_TYPE_FILE = 0x01;

	
	// Operation Status
	public static final int OPERATION_FAILED = 0x00;
	public static final int OPERATION_SUCCESS = 0x01;
	public static final int OPERATION_INPUT_ERROR = 0x02;
	
	public static final int SEARCH_CONTENT = 0x00;
	public static final int SEARCH_FILE_NAME = 0x01;
	
	public static final SimpleDateFormat DATE_TIME_FORMAT = new SimpleDateFormat("yyyy_MM_dd HH_mm_ss");
	
	public static final int INI_PARSER_NORMAL = 0x00;
	public static final int INI_PARSER_TEMPLATE = 0x01;
	
	public static int ADD_SECTION = 0x00;
	public static int ADD_KEY_VALUE = 0x01;
	public static int DELET_KEY_VALUE = 0x03;
	public static int UPDATE_VALUE = 0x04;
	public static int UNKNOWN = 0x05;
	
}
