package de.etas.tef.config.constant;

public interface IDataType
{	
	public static final String CHILD_TYPE = "type";
	public static final String DATA_DATA_TYPE = "dataType";
	public static final String DATA_ID = "id";
	public static final String DATA_VALUE = "value";
	public static final String DATA_FORMAT = "format";
	public static final String DATA_TYPE_DATE = "date";
	public static final String DATA_TYPE_LIST = "list";
}
