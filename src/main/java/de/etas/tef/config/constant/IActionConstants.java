package de.etas.tef.config.constant;

public interface IActionConstants
{
	public static final int ACTION_NEW_FILE_SELECTED = 0x00;
	public static final int ACTION_BLOCK_SELECTED = 0x15;
	public static final int ACTION_LOCK_SELECTION_CHANGED = 0x16;
	public static final int ACTION_DROP_NEW_FILE_SELECTED = 0x18;
	public static final int ACTION_PARAMETER_SELECTED = 0x1C;
	public static final int ACTION_SELECTED_SEARCH_PATH = 0x1E;
	public static final int ACTION_PROGRESS_BAR_DISPLAY = 0x20;
	public static final int ACTION_PROGRESS_BAR_UPDATE = 0x21;
	public static final int ACTION_OPEN_INI_FILE = 0x22;
	public static final int ACTION_PARSER_INI_FINISH = 0x23;
	public static final int ACTION_SEARCH_TYPE_CHANGED = 0x24;
	public static final int ACTION_SET_CONFIG_BLOCK = 0x25;
	public static final int ACTION_GET_FILE_HISTORY = 0x26;
	public static final int ACTION_SEARCH_CONTENT = 0x27;
	public static final int ACTION_TOOLBAR_ITEM = 0x28;
	public static final int ACTION_UPDATE_FILE_LIST_NUM = 0x29;
	public static final int ACTION_START_SEARCH = 0x30;
	public static final int ACTION_SEARCH_NEXT = 0x31;
	public static final int ACTION_GET_SEARCH_TEXT = 0x32;
	public static final int ACTION_GET_REPLACE_TEXT = 0x33;
	public static final int ACTION_SET_SEARCH_TEXT = 0x34;
	public static final int ACTION_SET_REPLACE_TEXT = 0x35;
	public static final int ACTION_REPLACE_TEXT = 0x36;
	public static final int ACTION_PROGRESS_BAR_HIDE = 0x37;
	public static final int ACTION_REFRESH_SEARCH = 0x38;
	public static final int ACTION_UPDATE_FILE_LIST = 0x39;
	public static final int ACTION_SET_CONFIG_FILE = 0x40;
	public static final int ACTION_SEARCH_START = 0x41;
	public static final int ACTION_VALIDATION_FINISHED = 0x42;
	public static final int ACTION_SHOW_PROBLEM = 0x43;
	public static final int ACTION_TEMPLATE_NOT_SET = 0x44;
	public static final int ACTION_ADD_LOG = 0x45;
	public static final int ACTION_UPDATE_SELECTION_FILES = 0x46;
	public static final int ACTION_SHOW_VIEW_PANLE = 0x47;
	public static final int ACTION_UPDATE_TEMPLATE_FILES = 0x48;
	public static final int ACTION_ABOUT_DIALOG_CLOSE = 0x49;
	public static final int ACTION_SHOW_REPLACE_PANEL = 0x50;
	public static final int ACTION_SET_SEARCH_STATUS = 0x51;
	public static final int ACTION_DIR_SET = 0x52;
	public static final int ACTION_SHOW_DETAIL_SEARCH = 0x53;
	public static final int ACTION_TEMPLATE_SET = 0x54;
	public static final int ACTION_DIR_NOT_SET = 0x55;
	public static final int ACTION_RELOAD_FILES = 0x56;
}
