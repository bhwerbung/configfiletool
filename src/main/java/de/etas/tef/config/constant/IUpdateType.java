package de.etas.tef.config.constant;

public interface IUpdateType
{
	public static final int UPDATE_TYPE_UNKNOWN = 0x99;
	public static final int UPDATE_TYPE_SECTION = 0x00;
	public static final int UPDATE_TYPE_KEY = 0x01;
	public static final int UPDATE_TYPE_VALUE = 0x02;
	
}
