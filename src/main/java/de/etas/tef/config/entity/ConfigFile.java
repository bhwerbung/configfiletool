package de.etas.tef.config.entity;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import de.etas.tef.config.constant.ITextConstants;
import de.etas.tef.config.helper.Utils;

public final class ConfigFile
{
	private Path filePath = null;
	private List<ConfigBlock> configBlocks = null;
	private List<String> comments = null;
	private Map<Integer, List<Problem>> problems = null;
	
	// key is the block and list is the parameters
	private Map<String, List<String>> mustHaveElement = null;
	private Map<String, List<String>> obsoleteElement = null;
	
	public static final String[] preExtreactedElements = {ITextConstants.TXT_ATTR_MUST_HAVE, ITextConstants.TXT_ATTR_OBSOLETE};
	
	public ConfigFile()
	{
		configBlocks = new ArrayList<ConfigBlock>();
	}
	
	public Map<String, List<String>> getMustHaveList()
	{
		return mustHaveElement;
	}
	
	public Map<String, List<String>> getObsoleteList()
	{
		return obsoleteElement;
	}
	
	public Path getFilePath()
	{
		return filePath;
	}
	public void setFilePath(Path filePath)
	{
		this.filePath = filePath;
	}
	public List<ConfigBlock> getConfigBlocks()
	{
		return configBlocks;
	}
	public void setConfigBlocks(List<ConfigBlock> configBlocks)
	{
		this.configBlocks = configBlocks;
	}
	public List<String> getComments()
	{
		return comments;
	}
	public void addComments(String comment)
	{
		if(comments == null)
		{
			comments = new ArrayList<String>();
		}
		
		comments.add(comment);
	}
	
	public void addConfigBlock(ConfigBlock cb)
	{
		if( null == cb )
		{
			return;
		}
		
		getConfigBlocks().add(cb);
	}
	
	public List<ConfigBlock> findConfigBlock(String name)
	{
		if(Utils.isStringEmpty(name))
		{
			return null;
		}
		
		Iterator<ConfigBlock> it = configBlocks.iterator();
		
		List<ConfigBlock> result = new ArrayList<ConfigBlock>();
		
		while(it.hasNext())
		{
			ConfigBlock cb = it.next();
			
			if(cb.getBlockName().equalsIgnoreCase(name))
			{
				result.add(cb);
			}
		}
		
		return result;
	}
	
	public Map<Integer, List<Problem>> getProblemList()
	{
		if(problems == null)
		{
			problems = new HashMap<Integer, List<Problem>>();
		}
		
		return problems;
	}
	
	public void addProblem(Problem p)
	{
		if(p == null)
		{
			return;
		}
		
		if(problems == null)
		{
			problems = new HashMap<Integer, List<Problem>>();
		}
		
		List<Problem> ps = problems.get(p.getProblemType());
		
		if(ps == null)
		{
			ps = new ArrayList<Problem>();
			problems.put(p.getProblemType(), ps);
		}
		
		ps.add(p);
	}
	
	public void removeProblem(Problem p)
	{
		if(problems == null || problems.isEmpty())
		{
			return;
		}
		
		List<Problem> ps = problems.get(p.getProblemType());
		
		if(ps == null)
		{
			return;
		}

		ps.remove(p);
	}
	
	public void getAttrElements()
	{
		if(mustHaveElement == null)
		{
			mustHaveElement = new HashMap<String, List<String>>();
		}
		
		if(obsoleteElement == null)
		{
			obsoleteElement = new HashMap<String, List<String>>();
		}

		extractElements();
	}
	
	private void extractElements()
	{
		Iterator<ConfigBlock> it = getConfigBlocks().iterator();
		
		while(it.hasNext())
		{
			ConfigBlock cb = it.next();
			
			for(String s : preExtreactedElements)
			{
				findAttrs(cb, s);
			}
		}
	}
	
	private void findAttrs(ConfigBlock cb, String attr)
	{
		Map<String, List<String>> result = null;
		
		if(attr.equals(ITextConstants.TXT_ATTR_MUST_HAVE))
		{
			result = mustHaveElement;
		}
		else if(attr.equals(ITextConstants.TXT_ATTR_OBSOLETE))
		{
			result = obsoleteElement;
		}

		if(result == null)
		{
			return;
		}
		
		if(cb.getAttributes().contains(attr))
		{
			result.put(cb.getBlockName(), new ArrayList<String>());
		}
		
		List<KeyValuePair> paras = cb.getAllParameters();
		
		if(paras == null || paras.isEmpty())
		{
			return;
		}
		
		Iterator<KeyValuePair> keys = paras.iterator();
		
		while(keys.hasNext())
		{
			KeyValuePair kvp = keys.next();
			
			if(kvp.getType() == KeyValuePair.TYPE_COMMENT || kvp.getType() == KeyValuePair.TYPE_UNKNOWN)
			{
				continue;
			}
			
			if(kvp.getAttributes().contains(attr))
			{
				List<String> parList = result.get(cb.getBlockName());
				
				if(parList == null)
				{
					parList = new ArrayList<String>();
					result.put(cb.getBlockName(), parList);
				}
				
				parList.add(kvp.getKey());
			}
		}
	}

	@Override
	public ConfigFile clone() throws CloneNotSupportedException
	{
		ConfigFile cf = new ConfigFile();
		
		cf.setFilePath(this.getFilePath());
		
		List<ConfigBlock> newList = new ArrayList<ConfigBlock>();
		List<String> commentList = new ArrayList<String>();
		
		Iterator<ConfigBlock> itcb = configBlocks.iterator();
		
		while(itcb.hasNext())
		{
			newList.add(itcb.next().clone());
		}
		
		if(comments != null)
		{
			Iterator<String> itc = comments.iterator();
			
			while(itc.hasNext())
			{
				commentList.add(itc.next());
			}
		}
		
		return cf;
	}
	
}
