package de.etas.tef.config.entity;

import de.etas.tef.config.constant.IProblemConstants;
import de.etas.tef.config.constant.ISymbolConstants;

public class MissingKeyProblem implements Problem
{
	private final String block;
	private final String key;
	
	public MissingKeyProblem(final String block, final String key)
	{
		this.block = block;
		this.key = key;
	}

	@Override
	public int getProblemType()
	{
		return IProblemConstants.PROBLEM_MISSING_KEY;
	}

	@Override
	public String getProblemDescription()
	{
		StringBuilder sb = new StringBuilder();
		sb.append(ISymbolConstants.SYMBOL_LEFT_BRACKET);
		sb.append(block);
		sb.append(ISymbolConstants.SYMBOL_RIGHT_BRACKET);
		sb.append(ISymbolConstants.SYMBOL_SPACE);
		sb.append(key);
		
		return sb.toString();
	}

	@Override
	public Object getProblem()
	{
		return key;
	}

}
