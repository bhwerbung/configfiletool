package de.etas.tef.config.entity;

import de.etas.tef.config.constant.IProblemConstants;
import de.etas.tef.config.constant.ISymbolConstants;

public class UnknownKeyProblem implements Problem
{
	private final String block;
	private final String key;
	private final String value;
	
	public UnknownKeyProblem(final String block, final String key, final String value)
	{
		this.block = block;
		this.key = key;
		this.value = value;
	}

	@Override
	public int getProblemType()
	{
		return IProblemConstants.PROBLEM_UNKNOWN_KEY;
	}

	@Override
	public String getProblemDescription()
	{
		StringBuilder sb = new StringBuilder();
		sb.append(ISymbolConstants.SYMBOL_LEFT_BRACKET);
		sb.append(block);
		sb.append(ISymbolConstants.SYMBOL_RIGHT_BRACKET);
		sb.append(ISymbolConstants.SYMBOL_SPACE);
		sb.append(key);
		sb.append(ISymbolConstants.SYMBOL_SPACE);
		sb.append(ISymbolConstants.SYMBOL_EQUAL);
		sb.append(ISymbolConstants.SYMBOL_SPACE);
		sb.append(value);
		
		return sb.toString();
	}

	@Override
	public Object getProblem()
	{
		return key;
	}

}
