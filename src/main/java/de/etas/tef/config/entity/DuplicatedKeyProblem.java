package de.etas.tef.config.entity;

import de.etas.tef.config.constant.IProblemConstants;
import de.etas.tef.config.constant.ISymbolConstants;

public class DuplicatedKeyProblem implements Problem 
{
	
	private final ConfigBlock cb;
	private final String key;
	private final String value;
	
	public DuplicatedKeyProblem(ConfigBlock cb, String key, String value)
	{
		this.cb = cb;
		this.key = key;
		this.value = value;
	}
	
	@Override
	public int getProblemType() 
	{
		return IProblemConstants.PROBLEM_DUPLICATED_KEY;
	}

	@Override
	public String getProblemDescription() 
	{
		StringBuilder sb = new StringBuilder();
		sb.append(ISymbolConstants.SYMBOL_LEFT_BRACKET);
		sb.append(cb.getBlockName());
		sb.append(ISymbolConstants.SYMBOL_RIGHT_BRACKET);
		sb.append("    ");
		sb.append(key);
		sb.append(" ");
		sb.append(ISymbolConstants.SYMBOL_EQUAL);
		sb.append(" ");
		sb.append(value);
		
		return sb.toString();
	}

	@Override
	public Object getProblem() 
	{
		return new Object[] {cb, key, value};
	}

}
