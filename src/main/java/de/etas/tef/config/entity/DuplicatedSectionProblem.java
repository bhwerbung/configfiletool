package de.etas.tef.config.entity;

import de.etas.tef.config.constant.IProblemConstants;
import de.etas.tef.config.constant.ISymbolConstants;

public class DuplicatedSectionProblem implements Problem 
{
	
	private final String cb;
	
	public DuplicatedSectionProblem(String cb)
	{
		this.cb = cb;
	}
	
	@Override
	public int getProblemType() 
	{
		return IProblemConstants.PROBLEM_DUPLICATED_SECTION;
	}

	@Override
	public String getProblemDescription() 
	{
		StringBuilder sb = new StringBuilder();
		sb.append(ISymbolConstants.SYMBOL_LEFT_BRACKET);
		sb.append(cb);
		sb.append(ISymbolConstants.SYMBOL_RIGHT_BRACKET);
		
		return sb.toString();
	}

	@Override
	public Object getProblem() 
	{
		return cb;
	}

}
