package de.etas.tef.config.entity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public abstract class AbstractConfigElement
{
	protected List<String> attributes = Collections.emptyList();
	
	protected boolean hasParameter(String parameter)
	{
		
		Iterator<String> it = getAttributes().iterator();

		while (it.hasNext())
		{
			if (it.next().equals(parameter))
			{
				return true;
			}
		}

		return false;
	}
	
	public List<String> getAttributes()
	{
		return attributes;
	}
	
	public void addAttribute(String kv)
	{
		if(attributes == null)
		{
			attributes = new ArrayList<String>();
		}
		
		attributes.add(kv);
	}
	
	public void setAttributeList(List<String> attris)
	{
		this.attributes = attris;
	}
	
	public abstract String getElementName();
}
