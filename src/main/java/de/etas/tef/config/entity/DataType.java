package de.etas.tef.config.entity;

import de.etas.tef.config.constant.IConstants;
import de.etas.tef.config.constant.ITextConstants;

public class DataType
{
	private int id = IConstants.EMPTY_INT;
	private String format = ITextConstants.TXT_EMPTY_STRING;
	private String dataType = ITextConstants.TXT_EMPTY_STRING;
	private String nodeName = ITextConstants.TXT_EMPTY_STRING;
	private String value = ITextConstants.TXT_EMPTY_STRING;
	
	public String getValue()
	{
		return value;
	}
	public void setValue(String value)
	{
		this.value = value;
	}
	public String getNodeName()
	{
		return nodeName;
	}
	public void setNodeName(String nodeName)
	{
		this.nodeName = nodeName;
	}
	public int getId()
	{
		return id;
	}
	public void setId(int id)
	{
		this.id = id;
	}

	public String getFormat()
	{
		return format;
	}
	public void setFormat(String format)
	{
		this.format = format;
	}
	public String getDataType()
	{
		return dataType;
	}
	public void setDataType(String dataType)
	{
		this.dataType = dataType;
	}
	
	
	
}
