package de.etas.tef.config.helper;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.widgets.Control;

import de.etas.tef.config.constant.IProblemConstants;
import de.etas.tef.config.constant.ISymbolConstants;
import de.etas.tef.config.constant.ITextConstants;
import de.etas.tef.config.entity.KeyValue;

public final class Utils
{
	public static final Path getCurrentPath()
	{
		return Paths.get(ITextConstants.TXT_EMPTY_STRING).toAbsolutePath();
	}
	
	public static String getFileNameWithoutExtendsion(String fileName)
	{
		return fileName.replaceFirst("[.][^.]+$", "");
	}

	public static KeyValue parserKeyValue(String s)
	{
		if (s == null || s.isEmpty())
		{
			return null;
		}

		int index = s.indexOf(ISymbolConstants.SYMBOL_EQUAL);

		if (index < 0)
		{
			return null;
		}

		return new KeyValue(s.substring(0, index), s.substring(index + 1, s.length()));
	}
	
	public static String convertCheckID2String(final int checkerID)
	{
		switch(checkerID)
		{
		case IProblemConstants.SEL_DUPLICATE:
			return IProblemConstants.TXT_CHECKER_DUPLICATED_PROBLEM;
		case IProblemConstants.SEL_MISSING:
			return IProblemConstants.TXT_CHECKER_MISSING_ITEM_PROBLEM;
		case IProblemConstants.SEL_OBSOLETE:
			return IProblemConstants.TXT_CHECKER_OBSOLETE_PROBLEM;
		case IProblemConstants.SEL_UNKNOWN:
			return IProblemConstants.TXT_CHECKER_UNKNOWN_PROBLEM;
		}
		
		return ITextConstants.TXT_UNKNOWN;
	}

	public static String removeSpace(String input)
	{
		if (input == null || input.isEmpty())
		{
			return ITextConstants.TXT_EMPTY_STRING;
		}

		if (input.contains(ISymbolConstants.SYMBOL_EQUAL))
		{
			StringTokenizer st = new StringTokenizer(input, ISymbolConstants.SYMBOL_EQUAL);
			StringBuilder s = new StringBuilder();
			int total = st.countTokens();
			int counter = 1;
			while (st.hasMoreTokens())
			{
				String key = st.nextToken().trim();
				s.append(key);
				if (counter < total)
				{
					s.append(ISymbolConstants.SYMBOL_EQUAL);
				}
				counter++;
			}

			return s.toString();
		}

		return input.trim();
	}

	public static boolean isContentSame(String input_1, String input_2)
	{
		if (input_1 == null && input_2 == null)
		{
			return true;
		}

		if (input_1.isEmpty() && input_2.isEmpty())
		{
			return true;
		}

		if (input_1 == null || input_2 == null)
		{
			return false;
		}

		if (input_1.isEmpty() || input_2.isEmpty())
		{
			return false;
		}

		return input_1.trim().equalsIgnoreCase(input_2.trim());
	}

	public static boolean validFile(Path filePath, boolean createNewFile)
	{
		LinkOption[] options = { LinkOption.NOFOLLOW_LINKS };

		if (Files.notExists(filePath, options) && createNewFile)
		{
			try
			{
				Files.createFile(filePath);
			} catch (IOException e)
			{
				e.printStackTrace();
			}
		}

		return true;
	}

	public static boolean isStringEmpty(String s)
	{
		if (s == null || s.isEmpty())
		{
			return true;
		}

		return false;
	}

	public static <T> boolean validList(List<T> input)
	{
		if (input == null || input.isEmpty())
		{
			return false;
		}

		return true;
	}
	
	public static <T, S> boolean validMap(Map<T, S> input)
	{
		if(input == null || input.isEmpty())
		{
			return false;
		}
		
		return true;
	}
	
	public static String getTimeStample()
	{
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy_MM_dd_HH_mm_ss");
		LocalDateTime now = LocalDateTime.now();
		return now.format(dtf);
	}
	
	public static String getTime()
	{
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm:ss");
		LocalDateTime now = LocalDateTime.now();
		return "["+now.format(dtf)+"]";
	}	
	
	public static void setCompositeFont(Control control, int type)
	{
		FontData fontData = control.getFont().getFontData()[0];
		Font font = new Font(control.getDisplay(), new FontData(fontData.getName(), fontData.getHeight(), type));
		control.setFont(font);
	}
	
	public static void setCompositeFont(Control control, int type, int size)
	{
		FontData fontData = control.getFont().getFontData()[0];
		fontData.setHeight(size);
		Font font = new Font(control.getDisplay(), new FontData(fontData.getName(), fontData.getHeight(), type));
		control.setFont(font);
	}
}
