package de.etas.tef.config.helper;

import java.util.Iterator;

import de.etas.tef.config.entity.ConfigBlock;
import de.etas.tef.config.entity.ConfigFile;

/**
 * Utility Tool for supporting {@code ConfigFile} Operations
 * 
 * @author UIH9FE
 *
 */
public final class ConfigFileUtils
{
	public static ConfigBlock findConfigBlockByName(ConfigFile cf, String blockName)
	{
		if(cf == null || blockName == null || blockName.isEmpty())
		{
			return null;
		}
		
		Iterator<ConfigBlock> it = cf.getConfigBlocks().iterator();
		
		while(it.hasNext())
		{
			ConfigBlock cb = it.next();
			
			if(cb.getBlockName().equals(blockName))
			{
				return cb; 
			}
		}
		
		return null;
	}
}
