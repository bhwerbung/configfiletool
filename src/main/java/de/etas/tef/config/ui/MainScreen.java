package de.etas.tef.config.ui;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Monitor;
import org.eclipse.swt.widgets.Shell;

import de.etas.tef.config.constant.IConstants;
import de.etas.tef.config.constant.IImageConstants;
import de.etas.tef.config.constant.ITextConstants;
import de.etas.tef.config.controller.MainController;
import de.etas.tef.config.listener.IMessageListener;
import de.etas.tef.editor.message.MessageManager;

public class MainScreen implements IMessageListener
{
	private final MainController controller;
	
	private StatusbarComponent statusBar;

	public MainScreen(final Display display, final MainController controller)
	{
		this.controller = controller;
		MessageManager.INSTANCE.addMessageListener(this);
		
		Shell shell = new Shell(display);
		shell.setText(ITextConstants.TXT_APP_TITLE);
		shell.setImage(controller.getImageFactory().getImage(IImageConstants.IMAGE_TITLE));
		
		initialShell(shell);
		new MenuComponent(shell, controller);
//		initToolbar(shell, controller);
		new MainDisplayComponent(shell, controller);
		
		statusBar = new StatusbarComponent(shell, SWT.BORDER, controller);
		
		Runnable timer = new Runnable()
		{
			@Override
			public void run()
			{
				statusBar.updateTime();
				display.timerExec(1000, this);
			}
		};
		display.timerExec(1000, timer);
		
		shell.open();
	}

//	private void initToolbar(Shell shell, MainController controller2)
//	{
//		ToolBar tb = new ToolBar(shell, SWT.NONE);
//		GridData gd = new GridData(GridData.FILL_HORIZONTAL);
//		tb.setLayoutData(gd);
//		tb.setBackground(controller.getColorFactory().getColorBackground());
//		
//		ToolItem ti = new ToolItem(tb, SWT.PUSH);
//		ti.setImage(controller.getImageFactory().getImage(IImageConstants.IMAGE_EXIT));
//		ti.setText("Exit");
//		ti.addSelectionListener(new SelectionAdapter()
//		{
//			
//			@Override
//			public void widgetSelected(SelectionEvent arg0)
//			{
//				controller.exit(shell);
//				
//			}
//		});
//	}

	/** Define the main window location */
	private void initialShell(Shell shell)
	{
		Monitor primary = shell.getDisplay().getPrimaryMonitor();
		Rectangle area = primary.getClientArea();
		shell.pack();
		shell.setBounds((Math.abs(area.width - IConstants.MAIN_SCREEN_WIDTH)) / 2,
				Math.abs((area.height - IConstants.MAIN_SCREEN_HEIGHT)) / 2, IConstants.MAIN_SCREEN_WIDTH,
				IConstants.MAIN_SCREEN_HEIGHT);
		shell.setBackground(controller.getColorFactory().getColorBackground());
		shell.addListener(SWT.Close, new Listener()
	    {
	        public void handleEvent(Event event)
	        {
	            event.doit = controller.toExit(shell);
	        }
	    });
		
	}
	
	@Override
	public void receivedAction(int type, Object content)
	{
	}
}
