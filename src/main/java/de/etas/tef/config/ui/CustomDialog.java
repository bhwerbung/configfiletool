package de.etas.tef.config.ui;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Monitor;
import org.eclipse.swt.widgets.Shell;

import de.etas.tef.config.controller.MainController;

public abstract class CustomDialog extends Dialog
{
	protected Shell shell;
	protected String title;
	protected final int buttonWidth = 70;
	protected MainController controller;
	
	public CustomDialog(final Shell shell, final String title, final MainController controller)
	{
		this(shell, SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL, title, controller);
		this.shell = shell;
		this.title = title;
		this.controller = controller;
	}
	
	public CustomDialog(final Shell parent, int style, final String title, final MainController controller)
	{
		super(parent, style);
		this.shell = parent;
		this.title = title;
		this.controller = controller;
	}

	public String[] open()
	{
		// Create the dialog window
		shell.setText(title);
		createContents();
		Monitor primary = shell.getDisplay().getPrimaryMonitor();
		Rectangle area = primary.getClientArea();
		shell.setBounds((Math.abs(area.width - getWidth())) / 2,
				Math.abs((area.height - getHeight())) / 2, getWidth(),
				getHeight());
		shell.open();
		
		shell.addDisposeListener(new DisposeListener()
		{
			
			@Override
			public void widgetDisposed(DisposeEvent arg0)
			{
				dialogClose();
				
			}
		});
		
//		shell.pack();
		Display display = shell.getDisplay();
		while (!shell.isDisposed())
		{
			if (!display.readAndDispatch())
			{
				display.sleep();
			}
		}
		// Return the entered value, or null
		
		return getResult();
	}
	
	protected abstract void dialogClose();

	protected abstract int getHeight();

	protected abstract int getWidth();

	protected abstract String[] getResult();

	protected abstract void createContents();
}
