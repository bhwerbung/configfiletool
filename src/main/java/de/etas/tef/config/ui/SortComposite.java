package de.etas.tef.config.ui;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.MessageBox;

import de.etas.tef.config.constant.IConstants;
import de.etas.tef.config.constant.ITextConstants;
import de.etas.tef.config.controller.MainController;
import de.etas.tef.config.helper.Utils;
import de.etas.tef.config.listener.ReorderBlocksListener;

public class SortComposite extends AbstractComposite
{
	private Button sortBlocks, run;
	private ReorderBlocksListener resortRunner = null;
	
	public SortComposite(Composite parent, int style, MainController controller)
	{
		super(parent, style, controller);
	}
	
	
	@Override
	protected void initComposite() 
	{
		GridLayout layout = new GridLayout(1, false);
		layout.marginTop = 3;
		layout.marginBottom = layout.marginLeft = layout.marginRight = layout.marginHeight = layout.marginWidth = 3;
		GridData gd = new GridData(GridData.FILL_BOTH);
		gd.grabExcessVerticalSpace = true;
		this.setLayout(layout);
		this.setLayoutData(gd);
	
		
		sortBlocks = new Button(this, SWT.RADIO);
		sortBlocks.setText(ITextConstants.TXT_TABLE_RIGHT_MENU_REORDER_CONFIG_BLOCKS);
		Utils.setCompositeFont(sortBlocks, SWT.BOLD);
		sortBlocks.setSelection(true);
		
		run = new Button(this, SWT.PUSH);
		gd = new GridData();
		gd.widthHint = IConstants.BTN_DEFAULT_WIDTH;
		gd.verticalAlignment = GridData.END;
		gd.grabExcessVerticalSpace = true;
		run.setLayoutData(gd);
		run.setText(ITextConstants.TXT_BTN_FUNCTION_RUN);
		run.setForeground(controller.getColorFactory().getColorWhite());
		run.setBackground(controller.getColorFactory().getColorButtonBlue());
		
		run.addSelectionListener(new SelectionAdapter()
		{
			
			@Override
			public void widgetSelected(SelectionEvent arg0)
			{
//				if(removeComment.getSelection() == true)
//				{
//					if(removeLineRunner == null)
//					{
//						removeLineRunner = new RemoveLineListener(controller, getShell(), ITextConstants.TXT_REMOVE_COMMENT_1);
//					}
//					
//					removeLineRunner.widgetSelected(null);
//				}
				if(sortBlocks.getSelection() == true)
				{
					if(resortRunner == null)
					{
						resortRunner = new ReorderBlocksListener(controller, getShell());
					}
					
					resortRunner.widgetSelected(null);
				}
				else
				{
					MessageBox mb = new MessageBox(parent.getShell(), SWT.ICON_WARNING | SWT.YES );
					mb.setText(ITextConstants.TXT_TITLE_NO_FUNCTION_SELECTED);
					mb.setMessage(ITextConstants.TXT_CONTENT_NO_FUNCTION_SELECTED);
					mb.open();
				}
			}
			
		});
		
	}
	
	@Override
	public void receivedAction(int type, Object content)
	{

	}

}
