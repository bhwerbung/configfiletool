package de.etas.tef.config.ui;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyleRange;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.program.Program;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Link;
import org.eclipse.swt.widgets.Monitor;
import org.eclipse.swt.widgets.Shell;

import de.etas.tef.config.constant.IActionConstants;
import de.etas.tef.config.constant.IImageConstants;
import de.etas.tef.config.constant.ITextConstants;
import de.etas.tef.config.controller.MainController;
import de.etas.tef.editor.message.MessageManager;

public class AboutDialog extends Dialog
{
	private final MainController controller;
	private final int width = 450;
	private final int height = 300;
	private final String TEXT_DISCLAIM = ""
			+ "License:\n"
			+ "- Icons Free License from icons8: <a href=\"https://icons8.com\">https://icons8.com</a>\n"
			+ "- OpenJDK 17 with GNU General Public License, version 2";
	private final String TEXT_TITLE = "ConfigFile Tool (Version " + ITextConstants.TXT_VERSION + ")\n\nCopyright ETAS GmbH - TEF";
	private Shell dialog;

	public AboutDialog(Shell parent, MainController controller)
	{
		super(parent);
		this.controller = controller;
	}

	public String open()
	{
		Shell parent = getParent();
		dialog = new Shell(parent, SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL);
		dialog.setSize(width, height);
		initialShell(dialog, width, height);
		dialog.setImage(controller.getImageFactory().getImage(IImageConstants.IMAGE_ABOUT));
		dialog.setText("About - ConfigFile Tool");
		initDialog(dialog);
		dialog.open();
		Display display = parent.getDisplay();
		while (!dialog.isDisposed())
		{
			if (!display.readAndDispatch())
				display.sleep();
		}
		MessageManager.INSTANCE.sendMessage(IActionConstants.ACTION_ABOUT_DIALOG_CLOSE, null);
		return "exit";
	}
	
	private void initDialog(Shell shell)
	{
		GridLayout layout = new GridLayout(2, false);
		layout.marginTop = layout.marginLeft = layout.marginHeight = layout.marginWidth = layout.horizontalSpacing = layout.verticalSpacing = 0;
		layout.marginBottom = layout.marginRight = 5;
		GridData gd = new GridData(GridData.FILL_BOTH);
		shell.setLayout(layout);
		shell.setLayoutData(gd);
		shell.setBackground(controller.getColorFactory().getColorBackground());
		
		Label imageLabel = new Label(shell, SWT.NONE);
		imageLabel.setImage(controller.getImageFactory().getImage(IImageConstants.IMAGE_ETAS));
		
		StyledText textBrowser = new StyledText(shell, SWT.WRAP);
		gd = new GridData(GridData.FILL_HORIZONTAL);
		textBrowser.setLayoutData(gd);
		textBrowser.setText(TEXT_TITLE);
		textBrowser.setLineIndent(0, 3, 30);
		StyleRange style1 = new StyleRange();
	    style1.start = 0;
	    style1.length = 31;
	    style1.fontStyle = SWT.BOLD;
	    style1.underline = true;
	    textBrowser.setStyleRange(style1);
	    FontData[] fD = textBrowser.getFont().getFontData();
	    fD[0].setHeight(9);
	    textBrowser.setFont( new Font(shell.getDisplay(),fD[0]));

	    Link license = new Link(shell, SWT.NONE);
	    license.setBackground(controller.getColorFactory().getColorBackground());
		gd = new GridData(GridData.FILL_BOTH);
		gd.horizontalSpan = 2;
		gd.horizontalIndent = 20;
		gd.verticalIndent = 20;
		license.setLayoutData(gd);
		license.setText(TEXT_DISCLAIM);
		license.addSelectionListener(new SelectionAdapter()  {
			 
            @Override
            public void widgetSelected(SelectionEvent e) {
                Program.launch("https://icons8.com");
            }
             
        });
		
		Button close = new Button(shell, SWT.NONE);
		close.setBackground(controller.getColorFactory().getColorBackground());
		gd = new GridData();
		gd.horizontalSpan = 2;
		gd.horizontalAlignment = GridData.END;
		gd.widthHint = 90;
		close.setLayoutData(gd);
		close.setText("Close");
		close.addSelectionListener(new SelectionAdapter()
		{
			
			@Override
			public void widgetSelected(SelectionEvent arg0)
			{
				dialog.dispose();
			}
		});
	}

	private void initialShell(Composite shell, int width, int height)
	{
		Monitor primary = shell.getDisplay().getPrimaryMonitor();
		Rectangle area = primary.getClientArea();
		shell.pack();
		shell.setBounds((Math.abs(area.width - width)) / 2,
				Math.abs((area.height - height)) / 2, width,
				height);
	}
}
