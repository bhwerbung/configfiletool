package de.etas.tef.config.ui;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.MenuAdapter;
import org.eclipse.swt.events.MenuEvent;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

import de.etas.tef.config.constant.IActionConstants;
import de.etas.tef.config.constant.IConstants;
import de.etas.tef.config.constant.IImageConstants;
import de.etas.tef.config.constant.ITextConstants;
import de.etas.tef.config.controller.MainController;
import de.etas.tef.config.entity.ConfigFilePath;
import de.etas.tef.config.entity.Problem;
import de.etas.tef.config.helper.Utils;
import de.etas.tef.config.listener.FileListMouseListener;
import de.etas.tef.config.listener.FileListTableKeyListener;
import de.etas.tef.config.listener.OpenFileSelectionListener;
import de.etas.tef.config.listener.OpenLocationSelectionListener;
import de.etas.tef.config.worker.search.SearchInfo;
import de.etas.tef.editor.message.MessageManager;

public class ConfigFileListComposite extends AbstractComposite
{

	private Table configFileList;
	private TabFolder tabFolder;
	private SashForm mainArea;
//	private Button search;
	private Button btnClearAll, btnSelectAll, btnReload;
	private Label tableLabel;
	private FileSearchComposite fileSearchComposite;
	private TabItem searchItem;
	
	private TabItem simpleReplaceItem = null;
	private Overlay overlay;
	private boolean isDetailSearchShow = false;

	public ConfigFileListComposite(Composite parent, int style, MainController controller)
	{
		super(parent, style, controller);
	}
	
	private void initialMainPanel()
	{
		mainArea = new SashForm(this, SWT.VERTICAL);
		mainArea.setBackground(controller.getColorFactory().getColorBackground());
		GridData gd = new GridData(GridData.FILL_BOTH);
		mainArea.setLayoutData(gd);
	}
	
	private void initSearchComposite()
	{
		
		tabFolder = new TabFolder(mainArea, SWT.NONE);
		GridData gd = new GridData(GridData.FILL_BOTH);
		tabFolder.setLayoutData(gd);

		

		TabItem validationItem = new TabItem(tabFolder, SWT.NULL);
		validationItem.setText(ITextConstants.TXT_CHECKER_TAB_NAME);
		validationItem.setControl(new CheckerComposite(tabFolder, SWT.NONE, controller));

		TabItem replaceItem = new TabItem(tabFolder, SWT.NULL);
		replaceItem.setText(ITextConstants.TXT_REPLACE_TAB_NAME);
		replaceItem.setControl(new EditComposite(tabFolder, SWT.NONE, controller));
		
		TabItem functionItem = new TabItem(tabFolder, SWT.NULL);
		functionItem.setText(ITextConstants.TXT_FUNCTION_TAB_NAME);
		functionItem.setControl(new SortComposite(tabFolder, SWT.NONE, controller));

	}
	
	private void createDetailSearchTab()
	{
		searchItem = new TabItem(tabFolder, SWT.NULL);
		searchItem.setText(ITextConstants.TXT_DETAIL_SEARCH_TAB_NAME);
		fileSearchComposite = new FileSearchComposite(tabFolder, SWT.NONE, controller);
		searchItem.setControl(fileSearchComposite);
		tabFolder.setSelection(tabFolder.getItemCount()-1);
		showOverlay(false);
	}
	

	private void initialTable() 
	{
		Composite tableComposite = new Composite(mainArea, SWT.NONE);
		tableComposite.setBackground(controller.getColorFactory().getColorBackground());
		GridLayout layout = new GridLayout(1, false);
		layout.marginTop = layout.marginBottom = layout.marginLeft = layout.marginRight = layout.marginHeight = layout.marginWidth = layout.horizontalSpacing = layout.verticalSpacing = 0;
		GridData gd = new GridData(GridData.FILL_HORIZONTAL);
		gd.horizontalAlignment = GridData.CENTER;
		tableComposite.setLayout(layout);
		tableComposite.setLayoutData(gd);
		tableComposite.setBackground(controller.getColorFactory().getColorBackground());
		
		Composite buttonComposite = new Composite(tableComposite, SWT.NONE);
		buttonComposite.setBackground(controller.getColorFactory().getColorBackground());
		layout = new GridLayout(4, false);
		layout.marginTop = layout.marginBottom = layout.marginLeft = layout.marginRight = layout.marginHeight = layout.marginWidth = layout.horizontalSpacing = layout.verticalSpacing = 3;
		gd = new GridData(GridData.FILL_HORIZONTAL);
		gd.horizontalAlignment = GridData.BEGINNING;
		buttonComposite.setLayout(layout);
		buttonComposite.setLayoutData(gd);
		buttonComposite.setBackground(controller.getColorFactory().getColorBackground());
		
		btnClearAll = new Button(buttonComposite, SWT.PUSH);
		btnClearAll.setText(ITextConstants.TXT_BTN_CLEAR_ALL);
		gd = new GridData();
		gd.widthHint = IConstants.BTN_DEFAULT_WIDTH;
		btnClearAll.setLayoutData(gd);
		btnClearAll.setForeground(controller.getColorFactory().getColorWhite());
		btnClearAll.setBackground(controller.getColorFactory().getColorButtonBlue());
		btnClearAll.setImage(controller.getImageFactory().getImage(IImageConstants.IMAGE_CHECKBOX_UNCHECKED));
		btnClearAll.addSelectionListener(new SelectionListener()
		{
			
			@Override
			public void widgetSelected(SelectionEvent arg0)
			{
				for (int m = 0; m < configFileList.getItemCount(); m++) {
	            	configFileList.getItems()[m].setChecked(false);
	            	configFileList.deselectAll();
	            }
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent arg0)
			{
				// TODO Auto-generated method stub
				
			}
		});
		
		btnSelectAll = new Button(buttonComposite, SWT.PUSH);
		btnSelectAll.setText(ITextConstants.TXT_BTN_SELECT_ALL);
		gd = new GridData();
		gd.widthHint = IConstants.BTN_DEFAULT_WIDTH;
		btnSelectAll.setLayoutData(gd);
		btnSelectAll.setForeground(controller.getColorFactory().getColorWhite());
		btnSelectAll.setBackground(controller.getColorFactory().getColorButtonBlue());
		btnSelectAll.setImage(controller.getImageFactory().getImage(IImageConstants.IMAGE_CHECKBOX_CHECKED));
		btnSelectAll.addSelectionListener(new SelectionListener()
		{
			
			@Override
			public void widgetSelected(SelectionEvent arg0)
			{
				for (int m = 0; m < configFileList.getItemCount(); m++) {
	            	configFileList.getItems()[m].setChecked(true);
	                configFileList.selectAll();
	            }
				
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent arg0)
			{
				// TODO Auto-generated method stub
				
			}
		});
		
		btnReload = new Button(buttonComposite, SWT.PUSH);
		btnReload.setText(ITextConstants.TXT_BTN_RELOAD);
		gd = new GridData();
		gd.widthHint = IConstants.BTN_DEFAULT_WIDTH;
		btnReload.setLayoutData(gd);
		btnReload.setForeground(controller.getColorFactory().getColorWhite());
		btnReload.setBackground(controller.getColorFactory().getColorButtonBlue());
		btnReload.setImage(controller.getImageFactory().getImage(IImageConstants.IMAGE_REFRESH));
		btnReload.addSelectionListener(new SelectionListener()
		{
			
			@Override
			public void widgetSelected(SelectionEvent arg0)
			{
				MessageManager.INSTANCE.sendMessage(IActionConstants.ACTION_RELOAD_FILES, buttonComposite);
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent arg0)
			{
				// TODO Auto-generated method stub
				
			}
		});

		tableLabel = new Label(buttonComposite, SWT.NONE);
		tableLabel.setText(ITextConstants.TXT_FILE_TABLE_CONTENT);
		tableLabel.setBackground(controller.getColorFactory().getColorBackground());
		tableLabel.setForeground(controller.getColorFactory().getColorBlue());
		Utils.setCompositeFont(tableLabel, SWT.ITALIC, 10);

		
		configFileList = new Table(tableComposite, SWT.V_SCROLL | SWT.H_SCROLL | SWT.FULL_SELECTION | SWT.MULTI | SWT.CHECK);
		gd = new GridData(GridData.FILL_BOTH);
		configFileList.setLayoutData(gd);
		configFileList.setHeaderVisible(true);
		configFileList.setLinesVisible(false);
		TableColumn tcName = new TableColumn(configFileList, SWT.NONE);
		tcName.setText(ITextConstants.TXT_FILE_NAME);
//		tcName.setImage(controller.getImageFactory().getImage(IImageConstants.IMAGE_CHECKBOX_UNCHECKED));
//		tcName.addListener(SWT.Selection, new Listener() {
//		    @Override
//		    public void handleEvent(Event event) {
//		        boolean checkBoxFlag = false;
//		        for (int i = 0; i < configFileList.getItemCount(); i++) {
//		            if (configFileList.getItems()[i].getChecked()) {
//		                checkBoxFlag = true;
//		            }
//		        }
//
//		        if (checkBoxFlag) {
//		            for (int m = 0; m < configFileList.getItemCount(); m++) {
//		            	configFileList.getItems()[m].setChecked(false);
//		            	tcName.setImage(controller.getImageFactory().getImage(IImageConstants.IMAGE_CHECKBOX_UNCHECKED));
//		            	configFileList.deselectAll();
//
//		            }
//		        } else {
//		            for (int m = 0; m < configFileList.getItemCount(); m++) {
//		            	configFileList.getItems()[m].setChecked(true);
//		                tcName.setImage(controller.getImageFactory().getImage(IImageConstants.IMAGE_CHECKBOX_CHECKED));
//		                configFileList.selectAll();
//		            }
//		        }
//
//		    }
//		});
		
		TableColumn tcDate = new TableColumn(configFileList, SWT.NONE);
		tcDate.setText(ITextConstants.TXT_LAST_MODIFIED_DATE);
		configFileList.addControlListener(new ControlAdapter()
		{

			@Override
			public void controlResized(ControlEvent event)
			{
				adjustColumnSize();
			}

		});

		configFileList.addKeyListener(new FileListTableKeyListener(configFileList));

		configFileList.addMouseListener(new FileListMouseListener());

		createRightMenu();
	}

	@Override
	protected void initComposite()
	{
		super.initComposite();
		initialMainPanel();
		initialTable();
		initSearchComposite();
//		initialButtons();
		resetComposite();
	}
	
	private boolean isDetailSearchEnable()
	{
		TabItem[] items = tabFolder.getItems();
		if(items == null || items.length <= 0)
		{
			return false;
		}
		
		for(int i = 0; i < items.length; i++)
		{
			TabItem ti = items[i];
			String name = ti.getText();
			if(name.equals(ITextConstants.TXT_DETAIL_SEARCH_TAB_NAME))
			{
				return true;
			}
		}
		
		return false;
	}
	
	private void initialButtons()
	{
		Composite buttonComposite = new Composite(this, SWT.NONE);
		buttonComposite.setBackground(controller.getColorFactory().getColorBackground());
		GridLayout layout = new GridLayout(1, false);
		layout.marginTop = layout.marginBottom = 3;
		layout.marginLeft = layout.marginRight = layout.marginHeight = layout.marginWidth = layout.horizontalSpacing = layout.verticalSpacing = 5;
		GridData gd = new GridData(GridData.FILL_HORIZONTAL);
		gd.horizontalAlignment = GridData.CENTER;
		buttonComposite.setLayout(layout);
		buttonComposite.setLayoutData(gd);
		buttonComposite.setBackground(controller.getColorFactory().getColorBackground());
		
//		search = new Button(buttonComposite, SWT.PUSH);
//		search.setText("Search");
//		gd = new GridData();
////		gd.horizontalAlignment = GridData.CENTER;
//		gd.widthHint = IConstants.BTN_DEFAULT_WIDTH;
//		search.setLayoutData(gd);
//		search.setForeground(controller.getColorFactory().getColorWhite());
//		search.setBackground(controller.getColorFactory().getColorButtonBlue());
//		search.addSelectionListener(new SelectionListener()
//		{
//			
//			@Override
//			public void widgetSelected(SelectionEvent arg0)
//			{
//				showOverlay(false);
//				SearchInfo si = new SearchInfo();
//				if(isDetailSearchEnable() && fileSearchComposite != null)
//				{
//					fileSearchComposite.getSearchInfo(si);
//				}
//				MessageManager.INSTANCE.sendMessage(IActionConstants.Action_SEARCH_START, si);
//			}
//			
//			@Override
//			public void widgetDefaultSelected(SelectionEvent arg0)
//			{
//				// TODO Auto-generated method stub
//				
//			}
//		});
		
//		detailSearch = new Button(buttonComposite, SWT.PUSH);
//		detailSearch.setText("Detail Search");
//		gd = new GridData();
//		gd.horizontalAlignment = GridData.CENTER;
//		gd.widthHint = IConstants.BTN_DEFAULT_WIDTH;
//		detailSearch.setLayoutData(gd);
		
//		search.setEnabled(false);
//		detailSearch.setEnabled(false);
	}

	private void setSimpleReplaceItem()
	{
		if(simpleReplaceItem == null)
		{
			simpleReplaceItem = new TabItem(tabFolder, SWT.NULL);
			simpleReplaceItem.setText("Simple Replace");
			simpleReplaceItem.setControl(new SimpleReplacementComposite(tabFolder, SWT.NONE, controller));
		}
		else
		{
			if(simpleReplaceItem != null)
			{
				simpleReplaceItem.dispose();
				simpleReplaceItem = null;
			}
		}
	}

	protected Menu createRightMenu()
	{
		Menu rightClickMenu = new Menu(configFileList);
		configFileList.setMenu(rightClickMenu);

		rightClickMenu.addMenuListener(new MenuAdapter()
		{
			@Override
			public void menuShown(MenuEvent e)
			{
				MenuItem[] items = rightClickMenu.getItems();

				for (int i = 0; i < items.length; i++)
				{
					items[i].dispose();
				}

				TableItem[] selections = configFileList.getSelection();

				if (selections == null || selections.length < 1)
				{
					return;
				}

				TableItem item = selections[0];

				if (null == item)
				{
					return;
				}

				MenuItem openFileLocation = new MenuItem(rightClickMenu, SWT.NONE);
				openFileLocation.setText("Open File Location");
				openFileLocation.setImage(controller.getImageFactory().getImage(IImageConstants.IMAGE_FOLDER));
				openFileLocation
						.addSelectionListener(new OpenLocationSelectionListener((ConfigFilePath) item.getData()));

				MenuItem openFile = new MenuItem(rightClickMenu, SWT.NONE);
				openFile.setText("Open File");
				openFile.setImage(controller.getImageFactory().getImage(IImageConstants.IMAGE_FILE));
				openFile.addSelectionListener(new OpenFileSelectionListener((ConfigFilePath) item.getData()));

				MenuItem deleteItem = new MenuItem(rightClickMenu, SWT.NONE);
				deleteItem.setText("Delete Item");
				deleteItem.setImage(controller.getImageFactory().getImage(IImageConstants.IMAGE_DELETE_FILE));
				deleteItem.addSelectionListener(new DeleteItemSelectionListener());
				
//				new MenuItem(rightClickMenu, SWT.SEPARATOR);
//				
//				MenuItem reorderBlocks = new MenuItem(rightClickMenu, SWT.NONE);
//				reorderBlocks.setText(ITextConstants.TXT_TABLE_RIGHT_MENU_REORDER_CONFIG_BLOCKS);
//				reorderBlocks.setImage(controller.getImageFactory().getImage(IImageConstants.IMAGE_REORDER));
//				reorderBlocks.addSelectionListener(new ReorderBlocksListener((ConfigFilePath) item.getData(), controller, configFileList.getShell()));
			}
		});

		return rightClickMenu;
	}

	class DeleteItemSelectionListener implements SelectionListener
	{

		@Override
		public void widgetDefaultSelected(SelectionEvent arg0)
		{

		}

		@Override
		public void widgetSelected(SelectionEvent event)
		{
			MessageBox mb = new MessageBox(configFileList.getShell(), SWT.ICON_WARNING | SWT.YES | SWT.NO);
			mb.setText("Delete Confirmation");
			mb.setMessage("Do you really want to Delete Files?");

			boolean done = mb.open() == SWT.YES;

			if (!done)
			{
				return;
			}

			TableItem[] item = configFileList.getSelection();

			if (null == item || item.length < 1)
			{
				return;
			}
			
			List<ConfigFilePath> deleteItems = new ArrayList<ConfigFilePath>();
			
			for(TableItem ti : item)
			{
				deleteItems.add((ConfigFilePath) ti.getData());
			}
			
			controller.deleteINIFiles(deleteItems);
			updateList(controller.getCurrentFileList());
		}
	}

	private void updateList(List<ConfigFilePath> files)
	{
		configFileList.removeAll();

		if (files == null )
		{
			files = new ArrayList<>();
		}

		for (ConfigFilePath p : files)
		{
			TableItem ti = new TableItem(configFileList, SWT.NONE);
			ti.setChecked(false);
			ti.setText(0, getDisplayString(p));
			ti.setText(1, IConstants.DATE_TIME_FORMAT.format(new Date(p.getPath().toFile().lastModified())));
			ti.setData(p);

			int bg = configFileList.getItemCount() % 2;
			if (bg == 1)
			{
				ti.setBackground(controller.getColorFactory().getColorLightBlue());
			}
		}

		adjustColumnSize();
		MessageManager.INSTANCE.sendMessage(IActionConstants.ACTION_UPDATE_FILE_LIST_NUM, files);
	}

	private String getDisplayString(ConfigFilePath p)
	{

		return p.getPath().toString();
	}

	private void adjustColumnSize()
	{
		Rectangle rect = configFileList.getClientArea();
		configFileList.getColumn(0).setWidth((int) (rect.width * 0.7));
		configFileList.getColumn(1).setWidth((int) (rect.width * 0.3));
	}

	@SuppressWarnings("unchecked")
	@Override
	public void receivedAction(int type, Object content)
	{
		if (type == IActionConstants.ACTION_SELECTED_SEARCH_PATH)
		{

		} else if (type == IActionConstants.ACTION_UPDATE_FILE_LIST)
		{
			List<ConfigFilePath> list = (List<ConfigFilePath>) content;
			controller.setCurrentFileList(list);
			updateList(list);

		} else if (type == IActionConstants.ACTION_VALIDATION_FINISHED)
		{
			int checkType = (int)content;
			
			checkList(Utils.convertCheckID2String(checkType), checkType);
		} 
		else if (type == IActionConstants.ACTION_REPLACE_TEXT)
		{
		}
		else if(type == IActionConstants.ACTION_UPDATE_SELECTION_FILES)
		{
			List<ConfigFilePath> items = getSelectedItems();
			
			if(items == null)
			{
				return;
			}
			
			controller.setCurrentFileList(items);
		}
		else if(type == IActionConstants.ACTION_SHOW_REPLACE_PANEL)
		{
			setSimpleReplaceItem();
		}
		else if(type == IActionConstants.ACTION_TEMPLATE_NOT_SET)
		{
			resetComposite();
		}
		else if(type == IActionConstants.ACTION_DIR_SET)
		{
			activeComposite();
			String txt = ((String)content);
			
			if(txt.isEmpty() || txt.equals(ITextConstants.TXT_SELECT_DIR))
			{
//				if(overlay == null)
//				{
//					overlay = new Overlay(mainArea);
//					overlay.setText("Select listed files in the table to edit.");
//					overlay.setForeground(controller.getColorFactory().getColorRed());
//				}
//				
//				showOverlay(true);
				
				// do nothing
			}
			else
			{
				SearchInfo si = new SearchInfo();
				if(isDetailSearchEnable() && fileSearchComposite != null)
				{
					fileSearchComposite.getSearchInfo(si);
				}
				
				MessageManager.INSTANCE.sendMessage(IActionConstants.ACTION_SEARCH_START, si);
			}
		}
		else if(type == IActionConstants.ACTION_SHOW_DETAIL_SEARCH)
		{
			showDetailSearch();
		}
		else if(type == IActionConstants.ACTION_DIR_NOT_SET)
		{
			resetComposite();
		}
	}
	
	private void showDetailSearch()
	{
		if(isDetailSearchShow)
		{
			fileSearchComposite.dispose();
			fileSearchComposite = null;
			searchItem.dispose();
			isDetailSearchShow = false;
			tabFolder.setSelection(0);
		}
		else
		{
			createDetailSearchTab();
			isDetailSearchShow = true;
		}
	}
	
	private void showOverlay(boolean show)
	{
		if(overlay == null)
		{
			return;
		}
		
		if(show && (!overlay.isShowing()))
		{
			overlay.show();
		}
		else if(overlay.isShowing())
		{
			overlay.remove();
		}
	}

	private void resetComposite()
	{
		tabFolder.setEnabled(false);
		configFileList.setEnabled(false);
		updateList(null);
		btnSelectAll.setEnabled(false);
		btnClearAll.setEnabled(false);
		btnReload.setEnabled(false);

		if(isDetailSearchEnable())
		{
			showDetailSearch();
		}
		
		if(overlay != null && overlay.isShowing())
		{
			overlay.remove();
		}
		mainArea.setWeights(new int[] {3, 1});
	}
	
	private void activeComposite()
	{
//		search.setEnabled(true);
//		detailSearch.setEnabled(true);
		btnSelectAll.setEnabled(true);
		btnClearAll.setEnabled(true);
		btnReload.setEnabled(true);
		configFileList.setEnabled(true);
		tabFolder.setEnabled(true);
		mainArea.setWeights(new int[] {3, 1});
	}

	private List<ConfigFilePath> getSelectedItems()
	{
		TableItem[] items = configFileList.getItems();
		
		if(items == null || items.length <= 0)
		{
			return null;
		}
		
		List<ConfigFilePath> files = new ArrayList<ConfigFilePath>();
		
		for(TableItem ti : items)
		{
			if(ti.getChecked())
			{
				files.add((ConfigFilePath) ti.getData());
			}
		}
		
		return files;
	}

	private void checkList(String checkDesc, int checkType)
	{
		TableItem[] items = configFileList.getItems();

		if (items == null || items.length < 1)
		{
			return;
		}
		
		List<ConfigFilePath> problemFiles = new ArrayList<ConfigFilePath>();

		for (TableItem ti : items)
		{
			if(!ti.getChecked())
			{
				continue;
			}
			
			ConfigFilePath cfp = (ConfigFilePath) ti.getData();

			Map<Integer, List<Problem>> problems = cfp.getConfigFile().getProblemList();

			if (problems != null && problems.size() > 0)
			{
				problemFiles.add(cfp);
			}
		}
		
		Shell newShell = new Shell(getShell(), SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL);
		
		ProblemDialog pd = new ProblemDialog(newShell, "Problem List - " + checkDesc, controller, checkType);
		pd.setProblemList(problemFiles);
		pd.open();
	}
}
