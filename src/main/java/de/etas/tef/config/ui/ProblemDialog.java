package de.etas.tef.config.ui;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MenuAdapter;
import org.eclipse.swt.events.MenuEvent;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;

import de.etas.tef.config.constant.IConstants;
import de.etas.tef.config.constant.IImageConstants;
import de.etas.tef.config.constant.IProblemConstants;
import de.etas.tef.config.controller.MainController;
import de.etas.tef.config.entity.ConfigFile;
import de.etas.tef.config.entity.ConfigFilePath;
import de.etas.tef.config.entity.Problem;
import de.etas.tef.config.listener.OpenFileSelectionListener;
import de.etas.tef.config.listener.OpenLocationSelectionListener;

public class ProblemDialog extends CustomDialog
{
	private List<ConfigFilePath> problemList = Collections.emptyList();
	private Tree problemTree = null;
	private TreeItem root;
	@SuppressWarnings("unused")
	private final int checkType;

	public ProblemDialog(Shell parent, String title, final MainController controller, int checkType)
	{
		super(parent, title, controller);
		this.checkType = checkType;
	}

	@Override
	protected int getHeight()
	{
		return 800;
	}

	@Override
	protected int getWidth()
	{
		return 800;
	}

	@Override
	protected String[] getResult()
	{
		return null;
	}

	@Override
	protected void createContents()
	{
		shell.setBackground(controller.getColorFactory().getColorBackground());
		shell.setLayout(new GridLayout(1, false));
		shell.setImage(controller.getImageFactory().getImage(IImageConstants.IMAGE_PROBLEM));
		InnenComposite ic = new InnenComposite(shell, SWT.NONE, controller);
		problemTree = new Tree(ic, SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL);
		GridData gd = new GridData(GridData.FILL_BOTH);
		gd.heightHint = IConstants.HEIGHT_HINT;
		problemTree.setLayoutData(gd);
		problemTree.setLinesVisible(true);
//		problemTree.setHeaderVisible(true);
		root = new TreeItem(problemTree, SWT.NONE);
		root.setText("Problems");
		root.setImage(controller.getImageFactory().getImage(IImageConstants.IMAGE_PROBLEM));
		problemTree.addSelectionListener(new SelectionListener()
		{
			@Override
			public void widgetSelected(SelectionEvent event)
			{
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent event)
			{
				
			}
		});
		
//		problemTree.addMouseListener(getMouseListener());
//		
		createRightMenu();
		
		updateProblemList();
	}

	public void setProblemList(List<ConfigFilePath> problemFiles)
	{
		this.problemList = problemFiles;
	}

	private void updateProblemList()
	{
		if(problemList == null || problemList.isEmpty())
		{
			return;
		}
		
		for(ConfigFilePath cfp : problemList)
		{
			if(cfp == null)
			{
				continue;
			}
			
			ConfigFile cf = cfp.getConfigFile();
			Map<Integer, List<Problem>> problems = cf.getProblemList();
			
			if(problems == null || problems.isEmpty())
			{
				continue;
			}
			
			TreeItem fileRoot = new TreeItem(root, SWT.NONE);
			fileRoot.setText(cfp.getConfigFile().getFilePath().toString());
			fileRoot.setData(cfp);
			fileRoot.setImage(controller.getImageFactory().getImage(IImageConstants.IMAGE_FILE));
			extractProblems(fileRoot, problems);
			fileRoot.setExpanded(true);
		}
		
		root.setExpanded(true);
	}
	
	private void extractProblems(TreeItem fileRoot, Map<Integer, List<Problem>> problems)
	{
		Set<Integer> problemType = problems.keySet();
		
		for(Integer type : problemType)
		{
			switch(type)
			{
				case IProblemConstants.PROBLEM_DUPLICATED_KEY:
					createProblem(fileRoot, problems.get(IProblemConstants.PROBLEM_DUPLICATED_KEY), controller.getColorFactory().getColorBlue(), IProblemConstants.TXT_PROBLEM_DUPLICATED_KEY, IProblemConstants.PROBLEM_DUPLICATED_KEY);
					break;
				case IProblemConstants.PROBLEM_DUPLICATED_SECTION:
					createProblem(fileRoot, problems.get(IProblemConstants.PROBLEM_DUPLICATED_SECTION), controller.getColorFactory().getColorDarkGreen(), IProblemConstants.TXT_PROBLEM_DUPLICATED_SECTION, IProblemConstants.PROBLEM_DUPLICATED_SECTION);
					break;
				case IProblemConstants.PROBLEM_MISSING_BLOCK:
					createProblem(fileRoot, problems.get(IProblemConstants.PROBLEM_MISSING_BLOCK), controller.getColorFactory().getColorDarkGreen(), IProblemConstants.TXT_PROBLEM_MISSING_SECTION, IProblemConstants.PROBLEM_MISSING_BLOCK);
					break;
				case IProblemConstants.PROBLEM_MISSING_KEY:
					createProblem(fileRoot, problems.get(IProblemConstants.PROBLEM_MISSING_KEY), controller.getColorFactory().getColorBlue(), IProblemConstants.TXT_PROBLEM_MISSING_KEY, IProblemConstants.PROBLEM_MISSING_KEY);
					break;
				case IProblemConstants.PROBLEM_OBSOLETE_BLOCK:
					createProblem(fileRoot, problems.get(IProblemConstants.PROBLEM_OBSOLETE_BLOCK), controller.getColorFactory().getColorDarkGreen(), IProblemConstants.TXT_PROBLEM_OBSOLETE_SECTION, IProblemConstants.PROBLEM_OBSOLETE_BLOCK);
					break;
				case IProblemConstants.PROBLEM_OBSOLETE_KEY:
					createProblem(fileRoot, problems.get(IProblemConstants.PROBLEM_OBSOLETE_KEY), controller.getColorFactory().getColorBlue(), IProblemConstants.TXT_PROBLEM_OBSOLETE_KEY, IProblemConstants.PROBLEM_OBSOLETE_KEY);
					break;
				case IProblemConstants.PROBLEM_UNKNOWN_BLOCK:
					createProblem(fileRoot, problems.get(IProblemConstants.PROBLEM_UNKNOWN_BLOCK), controller.getColorFactory().getColorDarkGreen(), IProblemConstants.TXT_PROBLEM_UNKNOWN_SECTION, IProblemConstants.PROBLEM_UNKNOWN_BLOCK);
					break;
				case IProblemConstants.PROBLEM_UNKNOWN_KEY:
					createProblem(fileRoot, problems.get(IProblemConstants.PROBLEM_UNKNOWN_KEY), controller.getColorFactory().getColorBlue(), IProblemConstants.TXT_PROBLEM_UNKNOWN_KEY, IProblemConstants.PROBLEM_UNKNOWN_KEY);
					break;
			}
		}
	}

	private void createProblem(TreeItem fileRoot, List<Problem> list, Color color, String title, int problemType)
	{
		TreeItem problemRoot = new TreeItem(fileRoot, SWT.NONE);
		problemRoot.setText(title);
		problemRoot.setForeground(color);
		
		for(Problem p : list)
		{
			TreeItem prob = new TreeItem(problemRoot, SWT.NONE);
			prob.setText(p.getProblemDescription());
			prob.setData(problemType);
		}
		
		problemRoot.setExpanded(true);
	}

	class InnenComposite extends AbstractComposite
	{

		public InnenComposite(Composite parent, int style, MainController controller)
		{
			super(parent, style, controller);
		}

		@Override
		public void receivedAction(int type, Object content)
		{
			
		}
	}

	@Override
	protected void dialogClose()
	{
		if(problemTree == null || root == null)
		{
			return;
		}
		problemTree.dispose();
	}
	
	protected Menu createRightMenu()
	{
		Menu rightClickMenu = new Menu(problemTree);
		problemTree.setMenu(rightClickMenu);

		rightClickMenu.addMenuListener(new MenuAdapter()
		{
			@Override
			public void menuShown(MenuEvent e)
			{
				MenuItem[] items = rightClickMenu.getItems();

				for (int i = 0; i < items.length; i++)
				{
					items[i].dispose();
				}

				TreeItem[] selections = problemTree.getSelection();

				if (selections == null || selections.length < 1)
				{
					return;
				}

				TreeItem item = selections[0];

				if (null == item)
				{
					return;
				}
				
				Object obj = item.getData();
				
				if(obj != null)
				{
					if( obj instanceof ConfigFilePath)
					{
						MenuItem openFileLocation = new MenuItem(rightClickMenu, SWT.NONE);
						openFileLocation.setText("Open File Location");
						openFileLocation.setImage(controller.getImageFactory().getImage(IImageConstants.IMAGE_FOLDER));
						openFileLocation
						.addSelectionListener(new OpenLocationSelectionListener((ConfigFilePath) item.getData()));
						
						MenuItem openFile = new MenuItem(rightClickMenu, SWT.NONE);
						openFile.setText("Open File");
						openFile.setImage(controller.getImageFactory().getImage(IImageConstants.IMAGE_FILE));
						openFile.addSelectionListener(new OpenFileSelectionListener((ConfigFilePath) item.getData()));
					}
					else if(obj instanceof Integer)
					{
						int value = (Integer)obj;
						
						if(value == IProblemConstants.PROBLEM_MISSING_BLOCK || 
								value == IProblemConstants.PROBLEM_MISSING_KEY ||
								value == IProblemConstants.PROBLEM_DUPLICATED_SECTION ||
								value == IProblemConstants.PROBLEM_DUPLICATED_KEY ||
								value == IProblemConstants.PROBLEM_OBSOLETE_BLOCK ||
								value == IProblemConstants.PROBLEM_OBSOLETE_KEY ||
								value == IProblemConstants.PROBLEM_UNKNOWN_BLOCK ||
								value == IProblemConstants.PROBLEM_UNKNOWN_KEY
						)
						{
							MenuItem openFileLocation = new MenuItem(rightClickMenu, SWT.NONE);
							openFileLocation.setText("Copy");
							openFileLocation.setImage(controller.getImageFactory().getImage(IImageConstants.IMAGE_FILE_COPY));
							openFileLocation.addSelectionListener(new SelectionListener() {
								
								@Override
								public void widgetSelected(SelectionEvent arg0) {
									controller.toClipboard(item.getText());
								}
								
								@Override
								public void widgetDefaultSelected(SelectionEvent arg0) {
									// TODO Auto-generated method stub
									
								}
							});
						}
					}
				}
				
			}
		});

		return rightClickMenu;
	}
}
