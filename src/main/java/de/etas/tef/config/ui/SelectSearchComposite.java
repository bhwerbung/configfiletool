package de.etas.tef.config.ui;

import java.nio.file.Paths;
import java.util.Iterator;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CCombo;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import de.etas.tef.config.constant.IActionConstants;
import de.etas.tef.config.constant.IImageConstants;
import de.etas.tef.config.constant.ITextConstants;
import de.etas.tef.config.controller.MainController;
import de.etas.tef.config.helper.Utils;
import de.etas.tef.config.worker.search.SearchInfo;
import de.etas.tef.editor.message.MessageManager;

/**
 * Selection components are for the definition of working directory
 * 
 * @author UIH9FE
 *
 */
public class SelectSearchComposite extends AbstractComposite
{

	private Label labelTemplate, labelDir;
	private CCombo comboTemplate;
//	private Button btnRefresh;

	private Text txtFileSelect;
	private Button btnFileSelect;

	private Composite idleComposite;
	private String oldPath = ITextConstants.TXT_EMPTY_STRING;
	
	protected SelectSearchComposite(Composite parent, int style, MainController controller)
	{
		super(parent, style, controller);
	}

	@Override
	protected void initComposite()
	{
		GridLayout layout = new GridLayout(4, false);
		layout.marginTop = layout.marginBottom = layout.marginLeft = layout.marginRight = layout.marginHeight = layout.marginWidth = 0;
		this.setLayout(layout);
		this.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		this.setBackground(controller.getColorFactory().getColorBackground());
		
		idleComposite = new Composite(this, SWT.NONE);
		GridData gd = new GridData(GridData.FILL_HORIZONTAL);
		gd.horizontalSpan = 4;
		gd.heightHint = 0;
		idleComposite.setLayoutData(gd);

		initTemplate();
		initSelection();
		updateStatus();
	}
	
	
	private void updateTemplateList()
	{
		comboTemplate.removeAll();
		comboTemplate.add(ITextConstants.TXT_EMPTY_STRING);
		Iterator<String> it = controller.getTemplateNames().iterator();
		while (it.hasNext())
		{
			comboTemplate.add(it.next());
		}
	}
	
	private void setDirWarning() {
		txtFileSelect.setText(ITextConstants.TXT_SELECT_DIR);
		txtFileSelect.setForeground(controller.getColorFactory().getColorRed());
	}
	
	private void setDirNormal() {
		txtFileSelect.setMessage("Select Directory");
		txtFileSelect.setForeground(controller.getColorFactory().getColorBlack());
	}
	
	private void setComboWarning() {
		comboTemplate.setText(ITextConstants.TXT_SELECT_TEMPLATE);
    	comboTemplate.setForeground(controller.getColorFactory().getColorRed());
    	Utils.setCompositeFont(labelTemplate, SWT.BOLD);
    	idleComposite.setFocus();
	}
	
	private void setComboNormal() {
    	comboTemplate.setForeground(controller.getColorFactory().getColorBlack());
    	Utils.setCompositeFont(labelTemplate, SWT.NORMAL);
	}

	private void initTemplate()
	{
		labelTemplate = new Label(this, SWT.NONE);
		labelTemplate.setBackground(controller.getColorFactory().getColorBackground());
		labelTemplate.setText(ITextConstants.TXT_TEMPLATE);
		Utils.setCompositeFont(labelTemplate, SWT.BOLD);

		comboTemplate = new CCombo(this, SWT.BORDER | SWT.READ_ONLY);
		GridData gd = new GridData(GridData.FILL_HORIZONTAL);
		gd.horizontalSpan = 3;
		comboTemplate.setLayoutData(gd);
		
		updateTemplateList();
		comboTemplate.addSelectionListener(new SelectionAdapter()
		{

			@Override
			public void widgetSelected(SelectionEvent even)
			{
				String text = comboTemplate.getText();

				if (!text.isEmpty())
				{
					controller.setTemplate(text);
					MessageManager.INSTANCE.sendMessage(IActionConstants.ACTION_TEMPLATE_SET, null);
				}
				
				updateStatus();
				
			}
		});
		
		comboTemplate.addFocusListener(new FocusListener() {
	        @Override
	        public void focusLost(FocusEvent e) {
	            String text = comboTemplate.getText();
	            if(text.isEmpty()) {
	            	setComboWarning();
	            }
	            Utils.setCompositeFont(labelTemplate, SWT.BOLD);
	        }

	        @Override
	        public void focusGained(FocusEvent e) {
	            String text = comboTemplate.getText();
	            if(text.equals(ITextConstants.TXT_SELECT_TEMPLATE)) {
	            	comboTemplate.setText(ITextConstants.TXT_EMPTY_STRING);
	            	setComboNormal();
	            }
	            Utils.setCompositeFont(labelTemplate, SWT.BOLD);
	        }
	    });

//		btnRefresh = new Button(this, SWT.PUSH);
//		gd = new GridData(22, 26);
//		btnRefresh.setLayoutData(gd);
//		btnRefresh.addPaintListener(new PaintListener()
//		{
//
//			@Override
//			public void paintControl(PaintEvent event)
//			{
//				event.gc.setBackground(controller.getColorFactory().getColorBackground());
//				event.gc.fillRectangle(event.x, event.y, event.width, event.height);
//				Image image = controller.getImageFactory().getImage(IImageConstants.IMAGE_REFRESH);
//				event.gc.drawImage(image, 0, 0);
//			}
//		});
//		btnRefresh.setBackground(controller.getColorFactory().getColorBackground());
//		Image image = controller.getImageFactory().getImage(IImageConstants.IMAGE_REFRESH);
//		btnRefresh.setImage(image);
//		btnRefresh.setToolTipText(ITextConstants.TXT_RELOAD_TEMPLATE);
//		
//		btnRefresh.addSelectionListener(new SelectionAdapter()
//		{
//			
//			@Override
//			public void widgetSelected(SelectionEvent arg0)
//			{
//				controller.updateTemplateFiles();
//			}
//		});
	}

	private void initSelection()
	{
		labelDir = new Label(this, SWT.NONE);
		labelDir.setText("Working Directory:");
		labelDir.setBackground(controller.getColorFactory().getColorBackground());
		Utils.setCompositeFont(labelDir, SWT.BOLD);
		
		txtFileSelect = new Text(this, SWT.SINGLE | SWT.BORDER);
		GridData txtGD = new GridData(GridData.FILL_HORIZONTAL);
		txtGD.horizontalSpan = 2;
		txtFileSelect.setLayoutData(txtGD);
		txtFileSelect.setEditable(false);
		txtFileSelect.setMessage("Select Directory");
		txtFileSelect.setBackground(controller.getColorFactory().getColorBackground());

		btnFileSelect = new Button(this, SWT.PUSH);
		GridData gd = new GridData(24, 26);
		btnFileSelect.setLayoutData(gd);
//		btnFileSelect.addPaintListener(new PaintListener()
//		{
//
//			@Override
//			public void paintControl(PaintEvent event)
//			{
//				event.gc.setBackground(controller.getColorFactory().getColorBackground());
//				event.gc.fillRectangle(event.x, event.y, event.width, event.height);
//				Image image = controller.getImageFactory().getImage(IImageConstants.IMAGE_FOLDER);
//				event.gc.drawImage(image, 0, 0);
//			}
//		});
		btnFileSelect.setBackground(controller.getColorFactory().getColorBackground());
		Image image = controller.getImageFactory().getImage(IImageConstants.IMAGE_FOLDER);
		btnFileSelect.setImage(image);

		btnFileSelect.addSelectionListener(new SelectionAdapter()
		{

			@Override
			public void widgetSelected(SelectionEvent arg0)
			{
				setCurrFilePath(fileSelector(getShell()));
				oldPath = txtFileSelect.getText();
			}

		});

		setSelectionStatus(false);
	}

	protected void setCurrFilePath(String currFilePath)
	{

		if (null == currFilePath)
		{
			currFilePath = ITextConstants.TXT_EMPTY_STRING;
		}

		txtFileSelect.setText(currFilePath);
		
		updateStatus();

	}

	protected String fileSelector(Shell shell)
	{
		DirectoryDialog fd = new DirectoryDialog(shell, SWT.APPLICATION_MODAL | SWT.OPEN);
		return fd.open();
	}

	private void updateStatus()
	{
		String temp = comboTemplate.getText();
		
		if(temp.isEmpty()) {
			setComboWarning();
			txtFileSelect.setText(ITextConstants.TXT_EMPTY_STRING);
			setSelectionStatus(false);
			oldPath = ITextConstants.TXT_EMPTY_STRING;
			MessageManager.INSTANCE.sendMessage(IActionConstants.ACTION_TEMPLATE_NOT_SET, null);
		}else
		{
			setSelectionStatus(true);
			setComboNormal();
			
			String path = txtFileSelect.getText();
			if(path.isEmpty() || path.equals(ITextConstants.TXT_SELECT_DIR))
			{
				setDirWarning();
				oldPath = ITextConstants.TXT_EMPTY_STRING;
				MessageManager.INSTANCE.sendMessage(IActionConstants.ACTION_DIR_NOT_SET, oldPath);
			}else
			{
				setDirNormal();
				MessageManager.INSTANCE.sendMessage(IActionConstants.ACTION_DIR_SET, path);
			}
		}
		Utils.setCompositeFont(labelTemplate, SWT.BOLD);
	}

	private void setSelectionStatus(boolean enable)
	{
		txtFileSelect.setEnabled(enable);
		btnFileSelect.setEnabled(enable);
	}

	@Override
	public void receivedAction(int type, Object content)
	{
		if(type == IActionConstants.ACTION_UPDATE_TEMPLATE_FILES)
		{
			updateTemplateList();
		}
		else if(type == IActionConstants.ACTION_SEARCH_START)
		{
			SearchInfo si = (SearchInfo)content;
			si.setStartPath(Paths.get(txtFileSelect.getText()));
			controller.search(si);
		}
		else if(type == IActionConstants.ACTION_RELOAD_FILES)
		{
			MessageManager.INSTANCE.sendMessage(IActionConstants.ACTION_DIR_SET, txtFileSelect.getText());
		}
	}
}
