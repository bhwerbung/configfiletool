package de.etas.tef.config.ui;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;

import de.etas.tef.config.constant.IActionConstants;
import de.etas.tef.config.constant.IConstants;
import de.etas.tef.config.constant.IProblemConstants;
import de.etas.tef.config.constant.ITextConstants;
import de.etas.tef.config.controller.MainController;
import de.etas.tef.config.helper.Utils;
import de.etas.tef.editor.message.MessageManager;

public class CheckerComposite extends AbstractComposite
{
	private Button checkDuplicated;
	private Button checkMissing;
	private Button checkObsolete;
	private Button checkUnknown;
	
	private Button run;
	
	public CheckerComposite(Composite parent, int style, MainController controller)
	{
		super(parent, style, controller);
	}
	
	public void setComponentColor(Color color)
	{
		this.setBackground(color);
		checkDuplicated.setBackground(color);
		checkMissing.setBackground(color);
		checkObsolete.setBackground(color);
		checkUnknown.setBackground(color);
	}
	
	@Override
	protected void initComposite() 
	{
		GridLayout layout = new GridLayout(1, false);
		layout.marginTop = 3;
		layout.marginBottom = layout.marginLeft = layout.marginRight = layout.marginHeight = layout.marginWidth = 3;
		GridData gd = new GridData(GridData.FILL_BOTH);
		gd.grabExcessVerticalSpace = true;
		this.setLayout(layout);
		this.setLayoutData(gd);
	
		
		checkDuplicated = new Button(this, SWT.RADIO);
		checkDuplicated.setText(ITextConstants.TXT_DUPLICATED_SECTION);
		Utils.setCompositeFont(checkDuplicated, SWT.BOLD);
		
		checkMissing = new Button(this, SWT.RADIO);
		checkMissing.setText(ITextConstants.TXT_MISSING_ITEM);
		Utils.setCompositeFont(checkMissing, SWT.BOLD);
		
		checkObsolete = new Button(this, SWT.RADIO);
		checkObsolete.setText(ITextConstants.TXT_OBSOLETE_ITEM);
		Utils.setCompositeFont(checkObsolete, SWT.BOLD);
		
		checkUnknown = new Button(this, SWT.RADIO);
		gd = new GridData(GridData.FILL_HORIZONTAL);
		checkUnknown.setLayoutData(gd);
		checkUnknown.setText(ITextConstants.TXT_UNKNOWN_ITEM);
		Utils.setCompositeFont(checkUnknown, SWT.BOLD);
		
		run = new Button(this, SWT.PUSH);
		gd = new GridData();
		gd.widthHint = IConstants.BTN_DEFAULT_WIDTH;
		gd.verticalAlignment = GridData.END;
		gd.grabExcessVerticalSpace = true;
		run.setLayoutData(gd);
		run.setText(ITextConstants.TXT_BTN_CHECKER_RUN);
		run.setForeground(controller.getColorFactory().getColorWhite());
		run.setBackground(controller.getColorFactory().getColorButtonBlue());
		
		run.addSelectionListener(new SelectionAdapter() 
		{
			
			@Override
			public void widgetSelected(SelectionEvent event) 
			{
				MessageManager.INSTANCE.sendMessage(IActionConstants.ACTION_UPDATE_SELECTION_FILES, null);
				int value = getSelChecker();
				controller.validating(value, run.getShell());
			}
			
		});
		
	}
	
	private int getSelChecker()
	{
		int dup = checkDuplicated.getSelection() == true ? IProblemConstants.SEL_DUPLICATE : 0;
		int miss = checkMissing.getSelection() == true ? IProblemConstants.SEL_MISSING : 0;
		int obs = checkObsolete.getSelection() == true ? IProblemConstants.SEL_OBSOLETE : 0;
		int unk = checkUnknown.getSelection() == true ? IProblemConstants.SEL_UNKNOWN : 0;
		
		return dup | miss | obs | unk;
	}
	
	@Override
	public void receivedAction(int type, Object content)
	{

	}

}
