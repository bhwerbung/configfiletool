package de.etas.tef.config.ui;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;

import de.etas.tef.config.constant.IActionConstants;
import de.etas.tef.config.constant.ITextConstants;
import de.etas.tef.config.controller.MainController;
import de.etas.tef.config.listener.CheckEnableSelectionListener;
import de.etas.tef.config.worker.search.SearchInfo;

public class FileSearchComposite extends AbstractComposite
{
	private Button checkSearchFileName;
	private Text txtSearchFileName;

	private Button checkSearchSectionName;
	private Text txtSearchSectionName;

	private Button checkSearchKeyName;
	private Text txtSearchKeyName;

	private Button checkSearchValueName;
	private Text txtSearchValueName;
	
	
	public FileSearchComposite(Composite parent, int style, MainController controller)
	{
		super(parent, style, controller);
	}
	
	@Override
	protected void initComposite() 
	{
		GridLayout layout = new GridLayout(2, false);
		layout.marginTop = layout.marginBottom = layout.marginLeft = layout.marginRight = layout.marginHeight = layout.marginWidth = 3;
		this.setLayout(layout);
		GridData gd = new GridData(GridData.FILL_HORIZONTAL);
		this.setLayoutData(gd);

//		ToolBar tb = new ToolBar(this, SWT.NONE);
//		gd = new GridData(GridData.FILL_HORIZONTAL);
//		gd.horizontalSpan = 2;
//		tb.setLayoutData(gd);
//		search = new ToolItem(tb, SWT.PUSH);
//		Image searchImage = controller.getImageFactory().getImage(IImageConstants.IMAGE_SEARCH);
//		search.setImage(searchImage);
//		search.addSelectionListener(new SelectionAdapter()
//		{
//
//			@Override
//			public void widgetSelected(SelectionEvent arg0)
//			{
//				SearchInfo si = getSearchInfo();
//				controller.search(si);
//			}
//		});

		checkSearchFileName = new Button(this, SWT.CHECK);
		checkSearchFileName.setText(ITextConstants.TXT_FILE_NAME);
		txtSearchFileName = new Text(this, SWT.BORDER);
		gd = new GridData(GridData.FILL_HORIZONTAL);
		txtSearchFileName.setLayoutData(gd);
		txtSearchFileName.setMessage("Search File Name");
		txtSearchFileName.setEnabled(false);
		checkSearchFileName.addSelectionListener(new CheckEnableSelectionListener(txtSearchFileName));

//		btnSearch = new Button(searchComposite, SWT.PUSH);
//		gd = new GridData(24, 24);
//		btnSearch.setLayoutData(gd);
//		btnSearch.setImage(searchImage);
//		btnSearch.addPaintListener(new PaintListener() {
//			
//			@Override
//			public void paintControl(PaintEvent event) {
//				event.gc.fillRectangle(event.x, event.y, event.width, event.height);
//				event.gc.drawImage(searchImage, 0, 0);
//				event.gc.setBackground(controller.getColorFactory().getColorBackground());
//			}
//		});
//		

//		btnSearch.setVisible(false);

		checkSearchSectionName = new Button(this, SWT.CHECK);
		checkSearchSectionName.setText(ITextConstants.TXT_SECTION);
		txtSearchSectionName = new Text(this, SWT.BORDER);
		gd = new GridData(GridData.FILL_HORIZONTAL);
		txtSearchSectionName.setLayoutData(gd);
		txtSearchSectionName.setMessage("Search Section Name");
		txtSearchSectionName.setEnabled(false);
		checkSearchSectionName.addSelectionListener(new CheckEnableSelectionListener(txtSearchSectionName));

		checkSearchKeyName = new Button(this, SWT.CHECK);
		checkSearchKeyName.setText(ITextConstants.TXT_KEY);
		txtSearchKeyName = new Text(this, SWT.BORDER);
		gd = new GridData(GridData.FILL_HORIZONTAL);
		txtSearchKeyName.setLayoutData(gd);
		txtSearchKeyName.setMessage("Search Key Name");
		txtSearchKeyName.setEnabled(false);
		checkSearchKeyName.addSelectionListener(new CheckEnableSelectionListener(txtSearchKeyName));

		checkSearchValueName = new Button(this, SWT.CHECK);
		checkSearchValueName.setText(ITextConstants.TXT_VALUE);
		txtSearchValueName = new Text(this, SWT.BORDER);
		gd = new GridData(GridData.FILL_HORIZONTAL);
		txtSearchValueName.setLayoutData(gd);
		txtSearchValueName.setMessage("Search Value Name");
		txtSearchValueName.setEnabled(false);
		checkSearchValueName.addSelectionListener(new CheckEnableSelectionListener(txtSearchValueName));
		
		setSearchStatus(true);

	}
	
	private void setSearchStatus(boolean enable)
	{
		this.setEnabled(enable);
		checkSearchFileName.setEnabled(enable);
		checkSearchKeyName.setEnabled(enable);
		checkSearchValueName.setEnabled(enable);
		checkSearchSectionName.setEnabled(enable);
//		search.setEnabled(enable);
	}
	
	public SearchInfo getSearchInfo(SearchInfo si)
	{


		if (checkSearchFileName.getSelection())
		{
			si.setFileName(txtSearchFileName.getText());
		}

		if (checkSearchSectionName.getSelection())
		{
			si.setSectionName(txtSearchSectionName.getText());
		}

		if (checkSearchKeyName.getSelection())
		{
			si.setKeyName(txtSearchKeyName.getText());
		}

		if (checkSearchValueName.getSelection())
		{
			si.setValueName(txtSearchValueName.getText());
		}

		return si;
	}
	
	@Override
	public void receivedAction(int type, Object content)
	{
		if(type == IActionConstants.ACTION_SET_SEARCH_STATUS)
		{
			setSearchStatus((boolean)content);
		}
	}

}
