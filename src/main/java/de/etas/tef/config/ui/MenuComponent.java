package de.etas.tef.config.ui;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;

import de.etas.tef.config.constant.IActionConstants;
import de.etas.tef.config.constant.IImageConstants;
import de.etas.tef.config.controller.MainController;
import de.etas.tef.config.listener.IMessageListener;
import de.etas.tef.editor.message.MessageManager;

public class MenuComponent implements IMessageListener
{
	private final Shell shell;
	private final MainController controller;
	private MenuItem detailSearchItem;

	public MenuComponent(final Shell shell, final MainController controller)
	{
		this.shell = shell;
		this.controller = controller;
		MessageManager.INSTANCE.addMessageListener(this);
		createMenu();
	}

	private void createMenu()
	{
		Menu menuBar = new Menu(shell, SWT.BAR);
		
		createFileMenu(menuBar);
		createFunctionMenu(menuBar);
		createAboutMenu(menuBar);
		shell.setMenuBar(menuBar);
	}
	
	private void createFileMenu(final Menu menuBar)
	{
		MenuItem fileMenuHeader = new MenuItem(menuBar, SWT.CASCADE);
		fileMenuHeader.setText("&File");

		Menu fileMenu = new Menu(shell, SWT.DROP_DOWN);
		fileMenuHeader.setMenu(fileMenu);
		
		MenuItem fileExitItem = new MenuItem(fileMenu, SWT.PUSH);
		fileExitItem.setText("E&xit");
		fileExitItem.setImage(controller.getImageFactory().getImage(IImageConstants.IMAGE_EXIT));
		fileExitItem.addSelectionListener(new SelectionAdapter()
		{
			@Override
			public void widgetSelected(SelectionEvent event)
			{
				controller.exit(shell);
			}
		});
	}
	
	private void createFunctionMenu(final Menu menuBar)
	{
		MenuItem functionMenuHeader = new MenuItem(menuBar, SWT.CASCADE);
		functionMenuHeader.setText("F&unction");

		Menu functionMenu = new Menu(shell, SWT.DROP_DOWN);
		functionMenuHeader.setMenu(functionMenu);
		
		detailSearchItem = new MenuItem(functionMenu, SWT.PUSH);
		detailSearchItem.setText("&Show/Hide Detail Search");
		detailSearchItem.setImage(controller.getImageFactory().getImage(IImageConstants.IMAGE_DETAIL_SEARCH));
		detailSearchItem.addSelectionListener(new SelectionAdapter()
		{

			@Override
			public void widgetSelected(SelectionEvent arg0)
			{
				MessageManager.INSTANCE.sendMessage(IActionConstants.ACTION_SHOW_DETAIL_SEARCH, null);
			}

		});
		
		detailSearchItem.setEnabled(false);
		
		MenuItem viewPanelItem = new MenuItem(functionMenu, SWT.PUSH);
		viewPanelItem.setText("&Show/Hide View Panel");
		viewPanelItem.setImage(controller.getImageFactory().getImage(IImageConstants.IMAGE_SHOW_HIDE));
		viewPanelItem.addSelectionListener(new SelectionAdapter()
		{

			@Override
			public void widgetSelected(SelectionEvent arg0)
			{
				MessageManager.INSTANCE.sendMessage(IActionConstants.ACTION_SHOW_VIEW_PANLE, null);
			}

		});
		
//		MenuItem replacePanelItem = new MenuItem(functionMenu, SWT.PUSH);
//		replacePanelItem.setText("&Show/Hide Simple Replace Tab");
//		replacePanelItem.setImage(controller.getImageFactory().getImage(IImageConstants.IMAGE_REPLACE_PANEL));
//		replacePanelItem.addSelectionListener(new SelectionAdapter()
//		{
//
//			@Override
//			public void widgetSelected(SelectionEvent arg0)
//			{
//				MessageManager.INSTANCE.sendMessage(IActionConstants.ACTION_SHOW_REPLACE_PANEL, null);
//			}
//
//		});
	}
	
//	private void createWindowMenu(final Menu menuBar)
//	{
//		MenuItem windowMenuHeader = new MenuItem(menuBar, SWT.CASCADE);
//		windowMenuHeader.setText("Window");
//
//		Menu windowMenu = new Menu(shell, SWT.DROP_DOWN);
//		windowMenuHeader.setMenu(windowMenu);
//
//		MenuItem leftPaneItem = new MenuItem(windowMenu, SWT.PUSH);
//		leftPaneItem.setText("Show/Hide Left Pane");
//		leftPaneItem.addSelectionListener(new SelectionListener()
//		{
//
//			@Override
//			public void widgetSelected(SelectionEvent arg0)
//			{
//			}
//
//			@Override
//			public void widgetDefaultSelected(SelectionEvent arg0)
//			{
//				// TODO Auto-generated method stub
//
//			}
//		});
//
//		MenuItem rightPaneItem = new MenuItem(windowMenu, SWT.PUSH);
//		rightPaneItem.setText("Show/Hide Right Pane");
//		rightPaneItem.addSelectionListener(new SelectionListener()
//		{
//
//			@Override
//			public void widgetSelected(SelectionEvent arg0)
//			{
//			}
//
//			@Override
//			public void widgetDefaultSelected(SelectionEvent arg0)
//			{
//				// TODO Auto-generated method stub
//
//			}
//		});
//
//		MenuItem showInfoPaneItem = new MenuItem(windowMenu, SWT.PUSH);
//		showInfoPaneItem.setText("&Show/Hide Info Pane");
//		showInfoPaneItem.addSelectionListener(new SelectionListener()
//		{
//
//			@Override
//			public void widgetSelected(SelectionEvent event)
//			{
//			}
//
//			@Override
//			public void widgetDefaultSelected(SelectionEvent arg0)
//			{
//				// TODO Auto-generated method stub
//
//			}
//		});
//	}
	
	private void createAboutMenu(final Menu menuBar)
	{
		MenuItem aboutMenuHeader = new MenuItem(menuBar, SWT.CASCADE);
		aboutMenuHeader.setText("&?");

		Menu aboutMenu = new Menu(shell, SWT.DROP_DOWN);
		aboutMenuHeader.setMenu(aboutMenu);

		MenuItem aboutItem = new MenuItem(aboutMenu, SWT.PUSH);
		aboutItem.setText("&About");
		aboutItem.setImage(controller.getImageFactory().getImage(IImageConstants.IMAGE_ABOUT));
		aboutItem.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				AboutDialog dialog = new AboutDialog(shell, controller);
				dialog.open();
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				
			}
		});
	}
	
	private void activeDetailSearchMenuItem(boolean active)
	{
		detailSearchItem.setEnabled(active);
	}

	@Override
	public void receivedAction(int type, Object content)
	{
		if(type == IActionConstants.ACTION_TEMPLATE_NOT_SET)
		{
			activeDetailSearchMenuItem(false);
		}
		else if(type == IActionConstants.ACTION_DIR_SET)
		{
			activeDetailSearchMenuItem(true);
		}
	}
}
