package de.etas.tef.config.ui;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.DateTime;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import de.etas.tef.config.constant.ITextConstants;
import de.etas.tef.config.controller.MainController;
import de.etas.tef.config.entity.DataType;

/**
 * Date Selection Composite
 * 
 * @author UIH9FE
 *
 */
public class DateSelectionComposite extends AbstractComposite
{

	private Text text;
	private Button selection;
	private DateTime calendar;
	private DataType dt;

	protected DateSelectionComposite(Composite parent, int style, MainController controller, DataType dt)
	{
		super(parent, style, controller);
		this.dt = dt;
	}

	@Override
	protected void initComposite()
	{
		GridLayout layout = new GridLayout(2, false);
		layout.marginTop = layout.marginBottom = layout.marginLeft = layout.marginRight = layout.marginHeight = layout.marginWidth = layout.horizontalSpacing = layout.verticalSpacing = 0;
		this.setLayout(layout);
		GridData gd = new GridData(GridData.FILL_HORIZONTAL);
		gd.horizontalIndent = gd.verticalIndent = 0;
		gd.heightHint = 18;
		this.setLayoutData(gd);
		this.setBackground(controller.getColorFactory().getColorBackground());

		initDateComposite();
	}

	private void initDateComposite()
	{

		text = new Text(this, SWT.NONE);
		GridData gd = new GridData(GridData.FILL_HORIZONTAL);
		gd.horizontalIndent = gd.verticalIndent = 0;
		gd.heightHint = 18;
		text.setLayoutData(gd);
		text.setEditable(false);

		selection = new Button(this, SWT.NONE);
		gd = new GridData();
		gd.horizontalIndent = gd.verticalIndent = 0;
		gd.heightHint = 18;
		gd.widthHint = 20;
		selection.setLayoutData(gd);
		selection.setText("...");
		selection.addSelectionListener(new SelectionAdapter()
		{
			@Override
			public void widgetSelected(SelectionEvent event)
			{
				final Shell dialog = new Shell(selection.getShell(), SWT.DIALOG_TRIM);
				dialog.setLayout(new GridLayout(1, false));
				dialog.setText("Date Selection");
				dialog.setBackground(controller.getColorFactory().getColorBackground());

				calendar = new DateTime(dialog, SWT.CALENDAR);

				Button ok = new Button(dialog, SWT.PUSH);
				ok.setText("OK");
				ok.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));
				ok.addSelectionListener(new SelectionAdapter()
				{
					public void widgetSelected(SelectionEvent e)
					{
						updateCalendar();
						dialog.close();
					}
				});
				
				ok.setBackground(controller.getColorFactory().getColorBackground());
				dialog.setDefaultButton(ok);
				dialog.pack();
				dialog.setLocation(((Control) event.widget).toDisplay(event.x, event.y));
				dialog.open();
			}
		});
	}
	
	private void updateCalendar()
	{
		Calendar cal = Calendar.getInstance();
	    cal.set( Calendar.YEAR, calendar.getYear() );
	    cal.set( Calendar.MONTH, calendar.getMonth() );
	    cal.set( Calendar.DAY_OF_MONTH, calendar.getDay() );
	    text.setText( new SimpleDateFormat( dt.getFormat() ).format( cal.getTime() ) );
	}

	@Override
	public void receivedAction(int type, Object content)
	{
	}

	public void clearValue()
	{
		text.setText(ITextConstants.TXT_EMPTY_STRING);
	}

	public String getValue()
	{
		return text.getText();
	}
}
