package de.etas.tef.config.worker.checker;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

import de.etas.tef.config.constant.ITextConstants;
import de.etas.tef.config.controller.MainController;
import de.etas.tef.config.entity.ConfigBlock;
import de.etas.tef.config.entity.ConfigFile;
import de.etas.tef.config.entity.ConfigFilePath;
import de.etas.tef.config.entity.KeyValuePair;
import de.etas.tef.config.entity.ObsoleteBlockProblem;
import de.etas.tef.config.entity.ObsoleteKeyProblem;

public class ObsoleteItemValidator extends AbstractChecker
{

	public ObsoleteItemValidator(MainController controller)
	{
		super(controller);
	}

	@Override
	public void check(ConfigFilePath file)
	{
		ConfigFile templateFile = controller.getCurrentTemplate().getConfigFile();

		if (templateFile == null)
		{
			return;
		}

		Set<String> obsoleteBlocks = templateFile.getObsoleteList().keySet();
		
		if(obsoleteBlocks == null || obsoleteBlocks.isEmpty())
		{
			return;
		}

		for (String block : obsoleteBlocks)
		{

			// check if the complete block is obsolete
			List<ConfigBlock> cbs = file.getConfigFile().findConfigBlock(block);
			
			if(cbs == null || cbs.isEmpty())
			{
				continue;
			}
			
			ConfigBlock cb = cbs.get(0);
			
			if( cb == null )
			{
				continue;
			}
			
			ConfigBlock templateBlock = templateFile.findConfigBlock(block).get(0);
			
			if( isBlockObsolete(templateBlock) )
			{
				file.getConfigFile().addProblem(new ObsoleteBlockProblem(block));
				continue;
			}

			// check each key value in this block is obsolete
			
			List<String> obsoleteKeys = templateFile.getObsoleteList().get(block);
			
			if(obsoleteKeys == null || obsoleteKeys.isEmpty())
			{
				continue;
			}
			
			Iterator<String> obKeyIt = obsoleteKeys.iterator();
			
			while(obKeyIt.hasNext())
			{
				
				String key = obKeyIt.next();
				List<KeyValuePair> kvps = cb.findParameter(key);
				if(kvps != null && !kvps.isEmpty())
				{
					file.getConfigFile().addProblem(new ObsoleteKeyProblem(block, key));
				}
			}
		}
	}

	private boolean isBlockObsolete(ConfigBlock cb)
	{
		List<String> attrs = cb.getAttributes();
		if(attrs == null || attrs.isEmpty())
		{
			return false;
		}
		
		return attrs.contains(ITextConstants.TXT_ATTR_OBSOLETE);
	}
}
