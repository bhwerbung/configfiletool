package de.etas.tef.config.worker.checker;

import java.util.Iterator;

import de.etas.tef.config.controller.MainController;
import de.etas.tef.config.entity.ConfigBlock;
import de.etas.tef.config.entity.ConfigFile;
import de.etas.tef.config.entity.ConfigFilePath;
import de.etas.tef.config.entity.KeyValuePair;
import de.etas.tef.config.entity.UnknownBlockProblem;
import de.etas.tef.config.entity.UnknownKeyProblem;

public class UnknownItemValidator extends AbstractChecker
{

	public UnknownItemValidator(MainController controller)
	{
		super(controller);
	}

	@Override
	public void check(ConfigFilePath file)
	{
		ConfigFile template = controller.getCurrentTemplate().getConfigFile();
		
		ConfigFile iniFile = file.getConfigFile();
		
		Iterator<ConfigBlock> itBlock = iniFile.getConfigBlocks().iterator();
		
		while(itBlock.hasNext())
		{
			ConfigBlock cb = itBlock.next();
			String blockName = cb.getBlockName();
			
			if(template.findConfigBlock(blockName).isEmpty())
			{
				iniFile.addProblem(new UnknownBlockProblem(blockName));
			}
			else
			{
				ConfigBlock tempcb = template.findConfigBlock(blockName).get(0);
				Iterator<KeyValuePair> itEntry = cb.getAllParameters().iterator();
				
				while(itEntry.hasNext())
				{
					KeyValuePair kvp = itEntry.next();
					
					int type = kvp.getType();
					
					if(type == KeyValuePair.TYPE_COMMENT || type == KeyValuePair.TYPE_UNKNOWN)
					{
						continue;
					}
					
					if(tempcb.findParameter(kvp.getKey()).isEmpty())
					{
						iniFile.addProblem(new UnknownKeyProblem(blockName, kvp.getKey(), kvp.getValue()));
					}
				}
			}
		}
	}
		
}
