package de.etas.tef.config.worker.operation;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Iterator;
import java.util.List;

import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import de.etas.tef.config.constant.IConstants;
import de.etas.tef.config.constant.IUpdateType;
import de.etas.tef.config.controller.MainController;
import de.etas.tef.config.entity.ChangeAction;
import de.etas.tef.config.entity.ConfigBlock;
import de.etas.tef.config.entity.ConfigFile;
import de.etas.tef.config.entity.ConfigFilePath;
import de.etas.tef.config.entity.KeyValuePair;
import de.etas.tef.config.helper.Utils;

public class SimpleTextReplaceWorker extends AbstractOperation
{
	private int updateType = IUpdateType.UPDATE_TYPE_UNKNOWN;

	public SimpleTextReplaceWorker(String[] content, Display display, MainController controller, Shell shell)
	{
		super(content, controller.getCurrentFileList(), display, controller, false, shell);

		String type = content[0];

		if (!Utils.isStringEmpty(type))
		{
//			if (type.equals(SimpleReplacementComposite.replaceTypeList[1]))
//			{
//				updateType = IUpdateType.UPDATE_TYPE_SECTION;
//			} else if (type.equals(SimpleReplacementComposite.replaceTypeList[2]))
//			{
//				updateType = IUpdateType.UPDATE_TYPE_KEY;
//			} else if (type.equals(SimpleReplacementComposite.replaceTypeList[3]))
//			{
//				updateType = IUpdateType.UPDATE_TYPE_VALUE;
//			}
		}
	}

	@Override
	protected void doOperation(ConfigFilePath cfp) throws IOException
	{
		ConfigFile cf = cfp.getConfigFile();

		if (cf == null)
		{
			return;
		}

		switch (updateType)
		{
		case IUpdateType.UPDATE_TYPE_SECTION:
			handleBlockNameReplacement(cf, cfp.getPath());
			break;
			
		case IUpdateType.UPDATE_TYPE_KEY:
			handleKeyNameReplacement(cf, cfp.getPath());
			break;
		
		case IUpdateType.UPDATE_TYPE_VALUE:
			handleValueNameReplacement(cf, cfp.getPath());
			break;
		
		}

	}

	private void handleValueNameReplacement(ConfigFile cf, Path path)
	{
		Iterator<ConfigBlock> blockIter = cf.getConfigBlocks().iterator();

		while (blockIter.hasNext())
		{
			ConfigBlock cb = blockIter.next();
			
			List<KeyValuePair> ps = cb.getAllParameters();

			if (ps != null && ps.size() > 0)
			{
				for (KeyValuePair kvp : ps)
				{
					if(kvp.getValue().equals(key))
					{
						kvp.setValue(value);
						replaceActionValue(cb, key, value, section, path);
					}
				}
			}
		}
	}

	private void handleKeyNameReplacement(ConfigFile cf, Path path)
	{
		Iterator<ConfigBlock> blockIter = cf.getConfigBlocks().iterator();

		while (blockIter.hasNext())
		{
			ConfigBlock cb = blockIter.next();
			
			List<KeyValuePair> ps = cb.findParameter(key);

			if (ps != null && ps.size() > 0)
			{
				for (KeyValuePair kvp : ps)
				{
					kvp.setKey(value);
					replaceActionKey(cb, key, value, section, path);
				}
			}
		}
	}

	private void replaceActionKey(ConfigBlock cb, String oldValue, String newValue, String type, Path path)
	{
		ChangeAction ca = new ChangeAction(IConstants.UPDATE_VALUE);
		ca.setFile(path);
		ca.setOldSection(cb.getBlockName());
		ca.setOldKey(oldValue);
		ca.setNewKey(newValue);
//		ca.setUpdateType(IUpdateType.UPDATE_TYPE_KEY);
		changes.add(ca);
		
	}
	
	private void replaceActionValue(ConfigBlock cb, String oldValue, String newValue, String type, Path path)
	{
		ChangeAction ca = new ChangeAction(IConstants.UPDATE_VALUE);
		ca.setFile(path);
		ca.setOldSection(cb.getBlockName());
		ca.setOldValue(oldValue);
		ca.setNewValue(newValue);
//		ca.setUpdateType(IUpdateType.UPDATE_TYPE_VALUE);
		changes.add(ca);
		
	}

	private void handleBlockNameReplacement(ConfigFile cf, Path path)
	{
		List<ConfigBlock> cbs = cf.findConfigBlock(key);

		if (cbs == null || cbs.isEmpty())
		{
			return;
		}

		Iterator<ConfigBlock> blockIter = cbs.iterator();

		while (blockIter.hasNext())
		{
			ConfigBlock cb = blockIter.next();
			cb.setBlockName(value);
			replaceActionSection(cb, key, value, section, path);

		}
	}

	private void replaceActionSection(ConfigBlock cb, String oldValue, String newValue, String type, Path path)
	{
		ChangeAction ca = new ChangeAction(IConstants.UPDATE_VALUE);
		ca.setFile(path);
		ca.setOldSection(oldValue);
		ca.setNewSection(newValue);
//		ca.setUpdateType(IUpdateType.UPDATE_TYPE_SECTION);
		changes.add(ca);
	}
}
