package de.etas.tef.config.worker.parser;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import de.etas.tef.config.constant.IDataType;
import de.etas.tef.config.constant.IFileConstants;
import de.etas.tef.config.controller.MainController;
import de.etas.tef.config.entity.DataType;

public final class DataTypeParser
{
	@SuppressWarnings("unused")
	private final MainController controller;

	public DataTypeParser(final MainController controller)
	{
		this.controller = controller;
	}

	public void parserDataType(Map<Integer, DataType> dataTypeList)
	{
		if (dataTypeList == null)
		{
			dataTypeList = new HashMap<Integer, DataType>();
		} else
		{
			dataTypeList.clear();
		}

		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

		try
		{
			dbf.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
			DocumentBuilder db = dbf.newDocumentBuilder();
			InputStream is = this.getClass().getClassLoader().getResourceAsStream(IFileConstants.FILE_DATA_TYPE_XML);
			Document doc = db.parse(is);
			doc.getDocumentElement().normalize();

			NodeList list = doc.getElementsByTagName(IDataType.CHILD_TYPE);

			if (list == null || list.getLength() <= 0)
			{
				return;
			}

			Node node = list.item(0);

			// find node type
			if (node.getNodeType() == Node.ELEMENT_NODE)
			{
				assignDataType(node, dataTypeList);
			}
			
		} catch (ParserConfigurationException | SAXException | IOException e)
		{
			e.printStackTrace();
		}
	}

	private void assignDataType(Node node, Map<Integer, DataType> dataTypeList)
	{
		NodeList children = node.getChildNodes();
		
		if(children != null && children.getLength() > 0)
		{
			for(int i = 0; i < children.getLength(); i++)
			{
				Node child = children.item(i);
				
				if(child.getNodeType() == Node.ELEMENT_NODE)
				{
					DataType dt = new DataType();
					
					dt.setNodeName(child.getNodeName());
					
					NodeList subChildren = child.getChildNodes();
					
					for(int j = 0; j < subChildren.getLength(); j++)
					{
						Node subChild = subChildren.item(j);
						
						if(subChild.getNodeType() == Node.ELEMENT_NODE)
						{
							String nodeName = subChild.getNodeName();
						
							if(nodeName.equals(IDataType.DATA_ID))
							{
								dt.setId(Integer.parseInt(subChild.getTextContent()));
							}
							else if(nodeName.equals(IDataType.DATA_DATA_TYPE))
							{
								dt.setDataType(subChild.getTextContent());
							}
							else if(nodeName.equals(IDataType.DATA_VALUE))
							{
								dt.setValue(subChild.getTextContent());
							}
							else if(nodeName.equals(IDataType.DATA_FORMAT))
							{
								dt.setFormat(subChild.getTextContent());
							}
						}
					}
					
					dataTypeList.put(dt.getId(), dt);
				}
			}
		}
	}
}
