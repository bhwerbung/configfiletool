package de.etas.tef.config.worker.checker;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import de.etas.tef.config.controller.MainController;
import de.etas.tef.config.entity.ConfigBlock;
import de.etas.tef.config.entity.ConfigFilePath;
import de.etas.tef.config.entity.MissingBlockProblem;
import de.etas.tef.config.entity.MissingKeyProblem;
import de.etas.tef.config.helper.ConfigFileUtils;

public class MissingItemValidator extends AbstractChecker
{

	public MissingItemValidator(MainController controller)
	{
		super(controller);
	}

	@Override
	public void check(ConfigFilePath file)
	{
		Map<String, List<String>> musthave = controller.getCurrentTemplate().getConfigFile().getMustHaveList();
		
		if(musthave == null || musthave.isEmpty())
		{
			return;
		}
		
		Set<String> mustBlocks = musthave.keySet();
		
		for(String block : mustBlocks)
		{
			ConfigBlock cb = ConfigFileUtils.findConfigBlockByName(file.getConfigFile(), block);
			
			if(cb != null)
			{
				Iterator<String> musthaveKeys = musthave.get(block).iterator();
				
				while(musthaveKeys.hasNext())
				{
					String key = musthaveKeys.next();
					
					if(cb.findParameter(key).isEmpty())
					{
						file.getConfigFile().addProblem(new MissingKeyProblem(block, key));
					}
				}
				
				continue;
			}
			
			MissingBlockProblem mbp = new MissingBlockProblem(block);
			file.getConfigFile().addProblem(mbp);
		}
		
		
	}
}
