package de.etas.tef.config.worker.parser;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import de.etas.tef.config.constant.ISymbolConstants;
/**
 * Paser for the Template INI File
 * 
 * [Section] <Attri_1, Attri_2...>
 * Key = Value <Attri_1, Attri_2...>
 * 
 * The comments line will be ignored (the line start with "--" and ";" will be ignored)
 * 
 * @author UIH9FE
 *
 */
public class TemplateIniFileParser extends AbstractINIFileParser
{

	@Override
	protected List<String> handleAttribute(String line) 
	{
		int attrStart = line.lastIndexOf(ISymbolConstants.SYMBOL_LEFT_ATTR_BRACKET);
		int attrEnd = line.lastIndexOf(ISymbolConstants.SYMBOL_RIGHT_ATTR_BRACKET);
		
		if(attrStart < 0 || attrEnd < 0 || attrEnd < attrStart)
		{
			return null;
		}
		
		String attr = line.substring(attrStart + 1, attrEnd);
		StringTokenizer st = new StringTokenizer(attr, ",");
		
		if(st.countTokens() < 0)
		{
			return null;
		}
		
		List<String> result = new ArrayList<String>();
		
		while(st.hasMoreTokens())
		{
			result.add(st.nextToken());
		}
		
		return result;
	}
}
