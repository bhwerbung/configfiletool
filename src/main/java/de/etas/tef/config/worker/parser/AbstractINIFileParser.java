package de.etas.tef.config.worker.parser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;

import de.etas.tef.config.constant.ISymbolConstants;
import de.etas.tef.config.entity.ConfigBlock;
import de.etas.tef.config.entity.ConfigFile;
import de.etas.tef.config.entity.KeyValuePair;

public abstract class AbstractINIFileParser
{
	public ConfigFile read(Path filePath)
	{
		ConfigFile configFile = new ConfigFile();
		configFile.setFilePath(filePath);

		try
		{
			Charset charset = Charset.forName("ISO-8859-1");
			List<String> lines = Files.readAllLines(filePath, charset);
			handleLines(configFile, lines);
				
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		return configFile;
	}
	
	public ConfigFile read(InputStream stream)
	{
		ConfigFile configFile = new ConfigFile();

		try
		{
			@SuppressWarnings("unused")
			Charset charset = Charset.forName("ISO-8859-1");
			BufferedReader br = new BufferedReader(new InputStreamReader(stream));
			String line = null;
			List<String> lines = new ArrayList<String>();
			
			while((line = br.readLine()) != null)
			{
				lines.add(line);
			}
			handleLines(configFile, lines);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		
		return configFile;
	}
	
	protected void handleLines(ConfigFile configFile, List<String> lines) 
	{
		ConfigBlock block = null;
		KeyValuePair pair = null;

		boolean fileStart = true;

		for (String s : lines)
		{
			s = s.trim();
			
			if(s.startsWith("MINRES_GND"))
			{
//				String t = s;
			}
			
			// comment for the whole config file
			if(fileStart)
			{
				if(s.isEmpty())
				{
					continue;
				}
				else if(isLineComment(s))
				{
					configFile.addComments(s);
				}
				else if(s.startsWith(ISymbolConstants.SYMBOL_LEFT_BRACKET))
				{
					fileStart = false;
					block = new ConfigBlock();
					block.setBlockName(extractBlockName(s));
					List<String> attrs = handleAttribute(s);
					if(attrs != null && !attrs.isEmpty())
					{
						block.setAttributeList(attrs);
					}
					configFile.addConfigBlock(block);
				}
				else
				{
//					LoggerController.INSTANCE().addIniFileFormatError(IErroReason.ERROR_INI_FILE_START, s);
				}
			}
			else
			{
				pair = new KeyValuePair();

				if (isLineComment(s))
				{
					pair.setKey(s);
					pair.setType(KeyValuePair.TYPE_COMMENT);
					block.addParameterInLast(pair);
				}
				else if (s.contains(ISymbolConstants.SYMBOL_EQUAL))
				{
					extractkeyValue(pair, s, block);
					List<String> attrs = handleAttribute(s);
					if(attrs != null && attrs.size() > 0)
					{
						pair.setAttributeList(attrs);
					}
					block.addParameterInLast(pair);
				}
				else if (s.startsWith(ISymbolConstants.SYMBOL_LEFT_BRACKET))
				{
					block = new ConfigBlock();
					block.setBlockName(extractBlockName(s));
//					System.out.println(block.getBlockName());
					configFile.addConfigBlock(block);
					List<String> attrs = handleAttribute(s);
					if(attrs != null && attrs.size() > 0)
					{
						block.setAttributeList(attrs);
					}
				}
				else
				{
//					LoggerController.INSTANCE().addIniFileFormatError(block, IErroReason.ERROR_INI_FILE_PARAMETER, s);
				}
			}
		}
	}
	
	protected abstract List<String> handleAttribute(String s);

	protected boolean isLineComment(String line)
	{
		return line.startsWith("--") || line.startsWith(";") || line.startsWith("#") || line.isEmpty();
	}
	
	protected String extractBlockName(String line)
	{
		int index = line.indexOf(ISymbolConstants.SYMBOL_RIGHT_BRACKET);
		
		return line.subSequence(1, index).toString();
	}
	
	protected void extractkeyValue(KeyValuePair kvp, String line, ConfigBlock block)
	{
//		if(line.contains(ISymbolConstants.SYMBOL_LEFT_ATTR_BRACKET))
//		{
//			line = line.substring(0, line.indexOf(ISymbolConstants.SYMBOL_LEFT_ATTR_BRACKET));
//		}
		
		StringTokenizer st = new StringTokenizer(line, ISymbolConstants.SYMBOL_EQUAL);
		
		if(st.countTokens() != 2)
		{
//			LoggerController.INSTANCE().addIniFileFormatError(block, IErroReason.ERROR_INI_FILE_PARAMETER, line);
		}
		
		int index = 0;
		StringBuilder sb = new StringBuilder();
		
		while(st.hasMoreTokens())
		{
			String token = st.nextToken();
			
			if(index == 0)
			{
				kvp.setKey(token);
			}
			else if(index == 1)
			{
				sb.append(token);
			}
			else
			{
				sb.append(ISymbolConstants.SYMBOL_EQUAL);
				sb.append(token);
			}
			
			index++;
		}
		
		kvp.setValue(sb.toString());
		kvp.setType(KeyValuePair.TYPE_PARA);
	}
	
	protected void printConfigFile(ConfigFile cf)
	{
		if(cf == null)
		{
//			System.out.println("Config File is NULL");
			return;
		}
		
//		System.out.println("== " + cf.getFilePath().toFile().getAbsolutePath() + " ==");
		
		Iterator<ConfigBlock> it = cf.getConfigBlocks().iterator();
		while(it.hasNext())
		{
			ConfigBlock cb = it.next();
//			System.out.println(IConstants.SYMBOL_LEFT_BRACKET + cb.getBlockName() + IConstants.SYMBOL_RIGHT_BRACKET);
			
			Iterator<KeyValuePair> itkvp = cb.getAllParameters().iterator();
			
			while(itkvp.hasNext())
			{
				KeyValuePair kvp = itkvp.next();
				
				int type = kvp.getType();
				
				switch(type)
				{
				case KeyValuePair.TYPE_COMMENT:
					System.out.println(kvp.getKey());
					break;
				case KeyValuePair.TYPE_PARA:
					System.out.println(kvp.getKey() + ISymbolConstants.SYMBOL_EQUAL + kvp.getValue());
					break;
				}
			}
			
//			System.out.println();
		}
	}
}
