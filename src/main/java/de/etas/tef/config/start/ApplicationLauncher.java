package de.etas.tef.config.start;

import de.etas.tef.config.controller.MainController;

public class ApplicationLauncher {
	
	private boolean isDevelopmentMode = true;
	
    public ApplicationLauncher()
    {
        Display display = new Display();
        new MainController(display, isDevelopmentMode);
        while((Display.getCurrent().getShells().length != 0)
                 && !Display.getCurrent().getShells()[0].isDisposed())
        {
             if(!display.readAndDispatch())
             {
                 display.sleep();
             }
        }       
    }
     
    public static void main(String[] args) 
    {
        new ApplicationLauncher();
    }
}
