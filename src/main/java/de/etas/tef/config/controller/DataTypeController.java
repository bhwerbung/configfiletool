package de.etas.tef.config.controller;

import java.util.HashMap;
import java.util.Map;

import de.etas.tef.config.entity.DataType;
import de.etas.tef.config.worker.parser.DataTypeParser;

public final class DataTypeController
{
	private Map<Integer, DataType> dataTypeList = new HashMap<Integer, DataType>();
	@SuppressWarnings("unused")
	private final MainController controller;
	private final DataTypeParser dataTypeParser;
	
	public DataTypeController(final MainController controller, final DataTypeParser dataTypeParser)
	{
		this.controller = controller;
		this.dataTypeParser = dataTypeParser;
		
		parser();
	}
	
	private void parser()
	{
		dataTypeParser.parserDataType(dataTypeList);
	}


	public DataType getDataType(int type)
	{
		 return dataTypeList.get(type);
	}
	
	public void clear()
	{
		dataTypeList.clear();
	}
}
