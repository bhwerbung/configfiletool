package de.etas.tef.config.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import de.etas.tef.config.constant.IConstants;
import de.etas.tef.config.constant.IFileConstants;
import de.etas.tef.config.entity.ConfigBlock;
import de.etas.tef.config.entity.ConfigFile;
import de.etas.tef.config.entity.ConfigFilePath;
import de.etas.tef.config.entity.KeyValuePair;
import de.etas.tef.config.helper.Utils;

/**
 * Load and store the template INI file
 * 
 * @author UIH9FE
 *
 */
public final class TemplateController {
	private ConfigFilePath currentTemplate;
	private Map<String, ConfigFilePath> templates = Collections.emptyMap();
	private MainController controller;
	private List<String> tempNames = new ArrayList<String>();
	private List<String> blocks = new ArrayList<String>();
	private List<String> keys = new ArrayList<String>();
	private final boolean isDevelopmentMode;
	private String fileDir = IFileConstants.FILE_DIR_RELEASE;

	public TemplateController(MainController controller, boolean isDevelopmentMode) {
		templates = new HashMap<String, ConfigFilePath>();
		this.controller = controller;
		this.isDevelopmentMode = isDevelopmentMode;
		loadAllTemplate();
	}

	public List<String> getTemplateNames() {
		return tempNames;
	}

	private Set<String> listFiles(String dir) {
		return Stream.of(new File(dir).listFiles()).filter(file -> !file.isDirectory()).map(File::getName)
				.collect(Collectors.toSet());
	}
	
	private void resetList()
	{
		tempNames.clear();
		templates.clear();
	}

	public void loadAllTemplate() {
		resetList();
		if(isDevelopmentMode)
		{
			fileDir = IFileConstants.FILE_DIR_DEV;
		}
		else
		{
			fileDir = IFileConstants.FILE_DIR_RELEASE;
		}
		
		Set<String> files = listFiles(fileDir);
		Iterator<String> it = files.iterator();
		while(it.hasNext())
		{
			String file = it.next();
			if(file.endsWith(".ini"))
			{
				try {
					loadSingleFile(fileDir + file, Utils.getFileNameWithoutExtendsion(file));
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	private void loadSingleFile(String file, String displayName) throws FileNotFoundException {
		// MCD Template
		InputStream is = null;
		if(isDevelopmentMode)
		{
			is = new FileInputStream(new File(file));
		}
		else
		{
			is = this.getClass().getClassLoader().getResourceAsStream(file);
		}
		ConfigFile cf = controller.getINIFileParser(IConstants.INI_PARSER_TEMPLATE).read(is);
		cf.getAttrElements();
		ConfigFilePath cfp = new ConfigFilePath(null);
		cfp.setConfigFile(cf);
		templates.put(displayName, cfp);
		tempNames.add(displayName);
	}

	public void setTemplate(String tempName) {
		currentTemplate = templates.get(tempName);
		if (currentTemplate != null) {
			updateBlocks();
		}
	}

	private void updateBlocks() {
		blocks.clear();
		if (currentTemplate == null) {
			return;
		}

		Iterator<ConfigBlock> it = currentTemplate.getConfigFile().getConfigBlocks().iterator();

		while (it.hasNext()) {
			blocks.add(it.next().getBlockName());
		}
	}

	public ConfigFilePath getTempate() {
		return currentTemplate;
	}

	public List<String> getTempBlockNames() {
		return blocks;
	}

	private ConfigBlock findBlock(String name) {
		Iterator<ConfigBlock> it = currentTemplate.getConfigFile().getConfigBlocks().iterator();

		while (it.hasNext()) {
			ConfigBlock cb = it.next();

			if (cb.getBlockName().equalsIgnoreCase(name)) {
				return cb;
			}
		}

		return null;
	}

	public List<String> getTempKeyNames(String block) {
		keys.clear();
		ConfigBlock cb = findBlock(block);

		if (cb == null) {
			return keys;
		}

		Iterator<KeyValuePair> it = cb.getAllParameters().iterator();

		while (it.hasNext()) {
			keys.add(it.next().getKey());
		}

		return keys;
	}

}
