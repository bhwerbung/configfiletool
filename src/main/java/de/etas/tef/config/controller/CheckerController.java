package de.etas.tef.config.controller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;

import de.etas.tef.config.constant.IActionConstants;
import de.etas.tef.config.constant.IProblemConstants;
import de.etas.tef.config.constant.ITextConstants;
import de.etas.tef.config.entity.ConfigFilePath;
import de.etas.tef.config.worker.checker.DuplicatedKeyValidator;
import de.etas.tef.config.worker.checker.DuplicatedSectionValidator;
import de.etas.tef.config.worker.checker.IChecker;
import de.etas.tef.config.worker.checker.MissingItemValidator;
import de.etas.tef.config.worker.checker.ObsoleteItemValidator;
import de.etas.tef.config.worker.checker.UnknownItemValidator;
import de.etas.tef.editor.message.MessageManager;

/**
 * Checker to validate the INI File.
 * 
 * Simple adding new checker and implement required interface
 * 
 * @author UIH9FE
 *
 */
public final class CheckerController 
{
	private final DuplicatedSectionValidator dSectionChecker;
	private final DuplicatedKeyValidator dKeyChecker;
	private final MissingItemValidator missChecker;
	private final ObsoleteItemValidator obsChecker;
	private final UnknownItemValidator unkChecker;
	private final Display display;
	private final MainController controller;
	
	public CheckerController(final Display display, final MainController controller) 
	{
		dSectionChecker = new DuplicatedSectionValidator(controller);
		dKeyChecker = new DuplicatedKeyValidator(controller);
		missChecker = new MissingItemValidator(controller);
		obsChecker = new ObsoleteItemValidator(controller);
		unkChecker = new UnknownItemValidator(controller);
		this.display = display;
		this.controller = controller;
	}

	/**
	 * check the problem of the INI Files
	 * 
	 * @param value
	 * @param currentFileList
	 * @param shell 
	 */
	public void check(int value, List<ConfigFilePath> currentFileList, Shell shell) 
	{
		List<IChecker> checkers = getChecker(value);
		
		if(checkers == null || checkers.isEmpty())
		{
			MessageBox mb = new MessageBox(shell, SWT.ICON_WARNING | SWT.YES);
			mb.setText(ITextConstants.TXT_TITLE_NO_CHECKER_SELECTED);
			mb.setMessage(ITextConstants.TXT_CONTENT_NO_CHECKER_SELECTED);
			mb.open();
			return;
		}
		
		Thread t = new Thread(new Runnable() 
		{
			
			@Override
			public void run() 
			{
				doValidating(checkers, currentFileList, value);
			}
		});
		
		t.start();
	}
	
	public String getCheckerName(int value)
	{
		return null;
	}
	
	private void doValidating(List<IChecker> checkers, List<ConfigFilePath> currentFileList, int value)
	{
		
		Iterator<ConfigFilePath> files = currentFileList.iterator();
		
		while(files.hasNext())
		{
			
			ConfigFilePath cf = files.next();
			cf = controller.reloadFile(cf);
			cf.getConfigFile().getProblemList().clear();
			Iterator<IChecker> it = checkers.iterator();
			
			while(it.hasNext())
			{
				IChecker checker = it.next();
				checker.check(cf);
			}
		}
		
		display.asyncExec(new Runnable() 
		{
			
			@Override
			public void run() 
			{
				MessageManager.INSTANCE.sendMessage(IActionConstants.ACTION_VALIDATION_FINISHED, value);
			}
		});
	}
	
	private List<IChecker> getChecker(int value)
	{
		if(value == IProblemConstants.SEL_NONE)
		{
			return null;
		}
		
		List<IChecker> results = new ArrayList<IChecker>();
		
		if((value & IProblemConstants.SEL_DUPLICATE) == IProblemConstants.SEL_DUPLICATE)
		{
			// add more checkers here
			results.add(dSectionChecker);
			results.add(dKeyChecker);
		}
		else if((value & IProblemConstants.SEL_MISSING) == IProblemConstants.SEL_MISSING)
		{
			results.add(missChecker);
		}
		else if((value & IProblemConstants.SEL_OBSOLETE) == IProblemConstants.SEL_OBSOLETE)
		{
			results.add(obsChecker);
		}
		else if((value & IProblemConstants.SEL_UNKNOWN) == IProblemConstants.SEL_UNKNOWN)
		{
			results.add(unkChecker);
		}
		
		return results;
	}
}