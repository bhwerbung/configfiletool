package de.etas.tef.config.listener;

import java.io.IOException;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;

import de.etas.tef.config.constant.IActionConstants;
import de.etas.tef.config.constant.ITextConstants;
import de.etas.tef.config.controller.MainController;
import de.etas.tef.config.entity.ConfigBlock;
import de.etas.tef.config.entity.ConfigFilePath;
import de.etas.tef.config.worker.operation.AbstractOperation;
import de.etas.tef.editor.message.MessageManager;

public class ReorderBlocksListener implements SelectionListener {

	private final MainController controller;
	private final Shell shell;
	
	public ReorderBlocksListener(final MainController controller, final Shell shell) {
		this.controller = controller;
		this.shell = shell;
	}
	
	@Override
	public void widgetDefaultSelected(SelectionEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void widgetSelected(SelectionEvent arg0) {
		
		MessageManager.INSTANCE.sendMessage(IActionConstants.ACTION_UPDATE_SELECTION_FILES, null);
		
		MessageBox mb = new MessageBox(shell, SWT.ICON_WARNING | SWT.YES | SWT.NO );
		mb.setText(ITextConstants.TXT_TITLE_RESORT_BLOCKS);
		mb.setMessage(ITextConstants.TXT_CONTENT_RESORT_BLOCKS);
		boolean shouldRun = mb.open() == SWT.YES;
		
		if(!shouldRun)
		{
			return;
		}
		
		List<ConfigFilePath> currentList = controller.getCurrentFileList();
		
		if(currentList == null || currentList.isEmpty())
		{
			mb = new MessageBox(shell, SWT.ICON_WARNING | SWT.YES );
			mb.setText(ITextConstants.TXT_TITLE_NO_FILE_SELECTED);
			mb.setMessage(ITextConstants.TXT_CONTENT_NO_FILE_SELECTED);
			mb.open();
			
			return;
		}
		
		Thread t = new Thread(new ReorderExecutor(new String[] {ITextConstants.TXT_EMPTY_STRING, ITextConstants.TXT_EMPTY_STRING, ITextConstants.TXT_EMPTY_STRING}, currentList, shell.getDisplay(), controller, shell));
		t.start();
	}
	
	class ReorderExecutor extends AbstractOperation
	{

		public ReorderExecutor(String[] content, List<ConfigFilePath> currentList, Display display,
				MainController controller, Shell shell) {
			super(content, currentList, display, controller, false, shell);
		}
		
		@Override
		public void run()
		{
			super.run();
			
			shell.getDisplay().asyncExec(new Runnable() {

				@Override
				public void run() {
					MessageBox mb = new MessageBox(shell, SWT.ICON_INFORMATION | SWT.YES );
					mb.setText("Reorder Blocks Finished");
					mb.setMessage("The Block Sequence is reordered as Template!");
					mb.open();
				}
				
			});
			

		}

		@Override
		protected void doOperation(ConfigFilePath cfp) throws IOException {
			
			List<ConfigBlock> newBlocks = controller.getSorter().sortConfigBlock(cfp.getConfigFile().getConfigBlocks());
			cfp.getConfigFile().setConfigBlocks(newBlocks);
		}
		
	}

}
