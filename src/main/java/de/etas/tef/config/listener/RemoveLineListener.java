package de.etas.tef.config.listener;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;

import de.etas.tef.config.constant.IActionConstants;
import de.etas.tef.config.constant.IConstants;
import de.etas.tef.config.constant.ITextConstants;
import de.etas.tef.config.controller.MainController;
import de.etas.tef.config.entity.ChangeAction;
import de.etas.tef.config.entity.ConfigBlock;
import de.etas.tef.config.entity.ConfigFile;
import de.etas.tef.config.entity.ConfigFilePath;
import de.etas.tef.config.entity.KeyValuePair;
import de.etas.tef.config.worker.operation.AbstractOperation;
import de.etas.tef.editor.message.MessageManager;

public class RemoveLineListener implements SelectionListener {

	private final MainController controller;
	private final Shell shell;
	private final String line;
	
	public RemoveLineListener(final MainController controller, final Shell shell, String line) {
		this.controller = controller;
		this.shell = shell;
		this.line = line;
	}
	
	@Override
	public void widgetDefaultSelected(SelectionEvent arg0) {

	}

	@Override
	public void widgetSelected(SelectionEvent arg0) {
		
		if(line == null || line.isEmpty())
		{
			return;
		}
		
		MessageManager.INSTANCE.sendMessage(IActionConstants.ACTION_UPDATE_SELECTION_FILES, null);
		
		MessageBox mb = new MessageBox(shell, SWT.ICON_WARNING | SWT.YES | SWT.NO );
		mb.setText(ITextConstants.TXT_TITLE_REMOVE_LINE);
		mb.setMessage(ITextConstants.TXT_CONTENT_REMOVE_LINE + line);
		boolean shouldRun = mb.open() == SWT.YES;
		
		if(!shouldRun)
		{
			return;
		}
		
		List<ConfigFilePath> currentList = controller.getCurrentFileList();
		
		if(currentList == null || currentList.isEmpty())
		{
			mb = new MessageBox(shell, SWT.ICON_WARNING | SWT.YES );
			mb.setText(ITextConstants.TXT_TITLE_NO_FILE_SELECTED);
			mb.setMessage(ITextConstants.TXT_CONTENT_NO_FILE_SELECTED);
			mb.open();
			
			return;
		}
		
		Thread t = new Thread(new RemoveLineExecutor(new String[] {ITextConstants.TXT_EMPTY_STRING, ITextConstants.TXT_EMPTY_STRING, ITextConstants.TXT_EMPTY_STRING}, currentList, shell.getDisplay(), controller, shell));
		t.start();
	}
	
	class RemoveLineExecutor extends AbstractOperation
	{

		public RemoveLineExecutor(String[] content, List<ConfigFilePath> currentList, Display display,
				MainController controller, Shell shell) {
			super(content, currentList, display, controller, false, shell);
		}
		
		@Override
		public void run()
		{
			super.run();
			
			shell.getDisplay().asyncExec(new Runnable() {

				@Override
				public void run() {
					MessageBox mb = new MessageBox(shell, SWT.ICON_INFORMATION | SWT.YES );
					mb.setText("Remove line finished");
					mb.setMessage("--=\"\" has been all removed.");
					mb.open();
				}
				
			});
		}

		@Override
		protected void doOperation(ConfigFilePath cfp) throws IOException {
			
			ConfigFile cf = cfp.getConfigFile();

			if (cf == null)
			{
				return;
			}

			List<ConfigBlock> cbs = cf.getConfigBlocks();

			if (cbs == null || cbs.isEmpty())
			{
				return;
			}
			
			for(ConfigBlock cb : cbs)
			{
				handleDeleteKeyValue(cb, cfp);
			}

		}
		
		private void handleDeleteKeyValue(ConfigBlock cb, ConfigFilePath p)
		{
			List<KeyValuePair> paras = cb.getAllParameters();
			
			if(paras == null || paras.isEmpty())
			{
				return;
			}
			
			Iterator<KeyValuePair> it = paras.iterator();
			
			while(it.hasNext())
			{
				KeyValuePair kvp = it.next();
				if(kvp.getType() == KeyValuePair.TYPE_COMMENT)
				{
					if(myTrim(kvp.getKey()).equals(line))
					{
						it.remove();
						deleteParaChangeAction(cb, kvp, p);
					}
				}
			}
			
		}
		
		private String myTrim(String input)
		{
			return input.replaceAll(" ", "");
		}
		
		private void deleteParaChangeAction(ConfigBlock cb, KeyValuePair kvp, ConfigFilePath cfp)
		{
			ChangeAction ca = new ChangeAction(IConstants.DELET_KEY_VALUE);
			ca.setOldSection(cb.getBlockName());
			ca.setOldKey(kvp.getKey());
			ca.setOldValue(kvp.getValue());
			ca.setFile(cfp.getPath());
			changes.add(ca);
		}
		
	}

}
